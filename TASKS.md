Tasks

- Create the entity Athlete

  - Attributes (done)
    Όνομα (string) firstName
    Επίθετο (string) lastName
    Όνομα Πατρός (string) fatherName
    Όνομα Μητρός (string) motherName
    Τηλέφωνο (string) telephone
    Διεύθυνση (string) address
    Email (string) email
    Πόλη (string) town
    ΤΚ (string) postalCode
    Ημερομηνία Γέννησης (date) birthDate
    ΑΜΚΑ (string) amka
    Αριθμός ταυτότητας (string) idNumber
    Σωματείο (string) association
    Ημερομηνία Εγγραφής (date) registrationDate
    Αριθμός Μητρώου (string) registrationNumber
    Αριθμός Άδειας (string) licenseNumber
    Ημερομηνία έκδοσης (date) issueDate
    Ημερομηνία Λήξης (date) expirationDate
    Εκδουσα Αρχή (string) issuingAuthority
  - Validations
    - All the attributes are required (done)
    - Check if email is correct
    - The birthdate is not allowed because under 18 years old
  - Endpoints
    - Total list
    - Paginated list (done)
    - Add an athlete (done)
    - Edit an athlete (done)
    - Delete an athlete (done)
    - Search an athlete (done)
  - Extra tasks
    - Create a JSON to represent the attribute name with the greek name. (done)

- Create the entity AthleteGun

  - Attributes (done)
    athleteID (number)
    Σειρά (number) order
    Αριθμός Όπλου (string) armsNumber
    Διαμέτρημα (string) caliber
  - Validations
    - All the attributes are required (done)
    - Validate caliber based on the list
  - Endpoints
    - Add gun for athlete
    - Delete gun for athlete
    - List guns based on the athlete
  - Extra tasks
    - Create a JSON to represent the attribute name with the greek name.

- Create the entity ShootingRange

  - Attributes (done)
    athleteID (number)
    Ημερομηνία (date only, no time) createdDate
    Έκδουσα Αρχή (string) issuingAuthority
    Διαμέτρημα (string) caliber
    Ποσότητα (integer) quantity
    Ποσότητα Αποθήκης (integer) warehouseQuantity
    Αγώνας (boolean) isContest
    Προπόνηση (boolean) isTraining
  - Validation
    - All the attributes are required (done)
    - Validate caliber based on the list
  - Endpoints
    - Add (done)
    - Edit (done)
    - Delete (done)
    - List unique dates from the table (done)
    - List by current date (done)
  - Extra tasks
    - Create a JSON to represent the attribute name with the greek name.

- Create the entity Ammo

  - Attributes (done)
    athleteID (number)
    Ημερομηνία (date) createdDate
    Τοποθεσία (string) location
    Διαμέτρημα (string) caliber
    Ποσότητα (integer) quantity
  - Validation
    - All the attributes are required (done)
    - Validate caliber based on the list
  - Endpoints
    - Add (done)
    - Edit (done)
    - Delete (done)
    - List unique dates from the table (done)
    - List all by date (done)
    - List by current date (done)
  - Extra tasks
    - Create a JSON to represent the attribute name with the greek name.

- UI Pages

  - Home page
    - Navigation menu that points to the page for the Athletes, Shooting Ranges and Ammo (done)
    - Show the number of the athletes
    - Show the dates for the shooting ranges that have been already submitted to the DB
      - Each one redirects to a table that can not be edited, deleted or add a new entry, however it will have a button to print
    - Show the dates for the ammos that have been already submitted to the DB
      - Each one redirects to a table that can not be edited, deleted or add a new entry, however it will have a button to print
  - Athletes
    - Button to add an athlete (done)
      - Shows a form on the screen (done)
    - Show the table of athletes (done)
      - There will be three buttons "edit", "delete" and "guns" (done)
        - For the "edit" will show the form with all the inputs filled (done)
        - For the "delete" will ask the user if he is sure (done)
        - For the "guns" (done)
          - will show a form to add a new gun (done)
          - under the form will contain a table with the athlete's guns (done)
          - each gun on the table the user can delete it (done)
          - the user will be able to change the order of the list (done)
  - Ammos

    - Button to add ammo based on the current date (done)
      - Shows a form on the screen (done)
        - The form will make the user to select first the Athlete (done)
        - After the selection will add information about the ammo (done)
    - The table for the current entries based on the date (done)
      - Each row will have a button to edit and delete (done)
        - For edit, will show a form on the screen (done)
        - For delete, will ask the user's permission (done)
    - Button to print the table in PDF will open a form to select
      - Horizontal or Orthogonal page
      - Select the attributes of the table to print
      - Select date or put everything

  - Shooting range
    - Button to add shooting range based on the current date (done)
      - Shows a form on the screen (done)
        - The form will make the user to select first the Athlete (done)
        - After the selection will add information about the shooting range (done)
    - The table for the current entries based on the date (done)
      - Each row will have a button to edit and delete (done)
        - For edit, will show a form on the screen (done)
        - For delete, will ask the user's permission (done)
    - Button to print in PDF will open a form to select
      - Horizontal or Orthogonal page
      - Select the attributes of the table to print
      - Select date or put everything

* Configuration
  - Attributes:
    association (string)
    dbFilename (string)
    dbFolder (string)
