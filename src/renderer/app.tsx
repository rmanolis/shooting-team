import * as React from "react";
import { Provider } from "mobx-react";
import { MemoryRouter, Switch, Route } from "react-router";
import { AmmoTablePage } from "./app/pages/ammo-table.page";
import { HomePage } from "./app/pages/home.page";
import { AthleteTableStore } from "./app/stores/athlete-table.store";
import { AthleteTablePage } from "./app/pages/athlete-table.page";
import { ShootingRangeTablePage } from "./app/pages/shooting-range-table.page";
import { ShootingRangeTableStore } from "./app/stores/shooting-range-table.store";
import { AmmoTableStore } from "./app/stores/ammo-table.store";

const stores = {
  athleteTableStore: new AthleteTableStore(),
  shootingRangeTableStore: new ShootingRangeTableStore(),
  ammoTableStore: new AmmoTableStore()
};

export class App extends React.Component<undefined, undefined> {
  render() {
    return (
      <Provider {...stores}>
        <MemoryRouter>
          <Switch>
            <Route path="/ammoTable" component={AmmoTablePage} />
            <Route path="/athleteTable" component={AthleteTablePage} />
            <Route
              path="/shootingRangeTable"
              component={ShootingRangeTablePage}
            />
            <Route path="/" component={HomePage} />
          </Switch>
        </MemoryRouter>
      </Provider>
    );
  }
}
