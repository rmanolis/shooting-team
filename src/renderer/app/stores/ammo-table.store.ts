import {
  AmmoI,
  InputAmmoI
} from "../../../main/ipc-server/models/ammo.interface";
import { observable, action } from "mobx";
import { AthleteI } from "../../../main/ipc-server/models/athlete.interface";
import {
  getAmmosPaginatedByDate,
  addInputAmmo,
  editInputAmmo,
  getAmmo,
  deleteAmmo
} from "../callers/ammo.caller";
import {
  isErrorI,
  ErrorI
} from "../../../main/ipc-server/models/common.interface";
import { DateEntityI } from "../../../main/ipc-server/models/date.interface";
import {
  getDateEntitiesPaginated,
  getTodayDate
} from "../callers/date-entity.caller";
import { getAthlete } from "../callers/athlete.caller";

export class AmmoTableStore {
  // attributes for the form to select dates
  @observable ammoDates: DateEntityI[] = [];
  @observable selectedAmmoDate?: DateEntityI = undefined;

  // variables for the table
  @observable ammoList: AmmoI[] = [];

  // attributes for the pagination
  @observable currentPage: number = 0;
  @observable sizePage: number = 10;
  @observable totalPages: number = 0;

  // attributes for the form to add or edit
  @observable selectedAthleteForInput?: AthleteI = undefined;
  @observable input: InputAmmoI = {
    athleteID: 0,
    dateEntityID: 0,
    caliber: "",
    quantity: 0,
    location: ""
  };
  @observable inputErrorFields = new Map<string, string>();

  // attributes to show the forms
  @observable isSelectDateFormOpened: boolean = false;
  @observable isInputFormOpened: boolean = false;
  @observable isDeleteFormOpened: boolean = false;
  @observable selectedAmmoForEditing?: number = undefined;

  @action
  async selectPageForAmmos(page: number) {
    const srd = this.selectedAmmoDate;
    if (srd) {
      const lmp = await getAmmosPaginatedByDate(srd.id, {
        page: page,
        size: this.sizePage
      });
      this.ammoList = lmp.data;
      this.currentPage = lmp.page;
      this.totalPages = lmp.totalPages;
      this.sizePage = lmp.size;
    }
  }

  @action
  changeSizePage = async (size: number) => {
    this.sizePage = size;
    await this.selectPageForAmmos(0);
  };

  @action
  closeModal = () => {
    this.isSelectDateFormOpened = false;
    this.isInputFormOpened = false;
    this.isDeleteFormOpened = false;
    this.selectedAmmoForEditing = undefined;
  };

  @action
  afterDateSelectionFromDateForm = (srd: DateEntityI) => {
    this.isSelectDateFormOpened = false;
    this.selectedAmmoDate = srd;
    this.selectedAmmoForEditing = undefined;
    this.selectPageForAmmos(0);
  };

  @action
  afterAddingAmmoFromInputForm = () => {
    this.isInputFormOpened = false;
    this.selectedAmmoForEditing = undefined;
    this.selectPageForAmmos(0);
  };

  @action
  pushAmmoDates = async (): Promise<boolean> => {
    let lastId: number | undefined = undefined;
    if (this.ammoDates.length > 0) {
      lastId = this.ammoDates[this.ammoDates.length - 1].id;
    }
    const dates = await getDateEntitiesPaginated(5, lastId);
    if (dates.length > 0) {
      this.ammoDates.push(...dates);
      return true;
    } else {
      return false;
    }
  };

  @action
  emptyInput() {
    this.input = {
      athleteID: 0,
      dateEntityID: 0,
      location: "",
      caliber: "",
      quantity: 0
    };
  }

  @action
  showFormToSelectDateModal = () => {
    this.isSelectDateFormOpened = true;
    this.ammoDates = [];
    this.pushAmmoDates();
  };

  @action
  async addInputAmmo() {
    this.input.dateEntityID = this.selectedAmmoDate!.id;
    const out = await addInputAmmo(this.input);
    if (isErrorI(out)) {
      const value = out as ErrorI;
      if (value.fields.length > 0) {
        value.fields.forEach(v => {
          this.inputErrorFields.set(v.field, v.message);
        });
      }
    } else {
      this.afterAddingAmmoFromInputForm();
      this.emptyInput();
      this.selectedAthleteForInput = undefined;
    }
  }

  @action
  async editInputAmmo() {
    this.input.dateEntityID = this.selectedAmmoDate!.id;
    const out = await editInputAmmo(this.selectedAmmoForEditing!, this.input);
    if (isErrorI(out)) {
      const value = out as ErrorI;
      if (value.fields.length > 0) {
        value.fields.forEach(v => {
          this.inputErrorFields.set(v.field, v.message);
        });
      }
    } else {
      this.afterAddingAmmoFromInputForm();
      this.emptyInput();
      this.selectedAthleteForInput = undefined;
    }
  }

  @action
  async setAmmoDateFromToday() {
    const today = await getTodayDate();
    if (isErrorI(today)) {
      console.log("Error setAmmoDateFromToday: ", today);
    } else {
      const td = today as DateEntityI;
      this.selectedAmmoDate = td;
    }
  }

  @action
  showDeleteForm = (id: number) => {
    return () => {
      this.selectedAmmoForEditing = id;
      this.isDeleteFormOpened = true;
    };
  };

  @action
  deleteAmmo = async () => {
    const selected = this.selectedAmmoForEditing;
    if (selected) {
      await deleteAmmo(selected);
      await this.selectPageForAmmos(0);
      console.log("deleted ammo ", selected);
      this.selectedAmmoForEditing = undefined;
    } else {
      console.log("Error: this.selectedAmmoForEditing is undefined");
    }
    this.isDeleteFormOpened = false;
  };

  @action
  showInputForm = (id?: number) => {
    return async () => {
      if (id) {
        const sr = await getAmmo(id);
        this.input = {
          athleteID: sr.athleteID,
          dateEntityID: sr.dateEntityID,
          caliber: sr.caliber,
          quantity: sr.quantity,
          location: sr.location
        };
        const athlete = await getAthlete(sr.athleteID);
        if (isErrorI(athlete)) {
          console.log("Error from getAthlete: ", athlete);
        } else {
          const ath = athlete as AthleteI;
          this.selectedAthleteForInput = ath;
          this.isInputFormOpened = true;
          this.selectedAmmoForEditing = id;
        }
      } else {
        this.selectedAmmoForEditing = undefined;
        this.isInputFormOpened = true;
      }
    };
  };
}
