import { observable, action } from "mobx";
import {
  AthleteI,
  InputAthleteI
} from "../../../main/ipc-server/models/athlete.interface";

import {
  ErrorI,
  isErrorI
} from "../../../main/ipc-server/models/common.interface";
import {
  getAthletesPaginated,
  getAthlete,
  deleteAthlete,
  addAthlete,
  editAthlete
} from "../callers/athlete.caller";
import {
  AthleteGunI,
  InputAthleteGunI
} from "../../../main/ipc-server/models/athlete-gun.interface";
import {
  getGunsByAthlete,
  deleteAthleteGun,
  addAthleteGun
} from "../callers/athlete-gun.caller";

export class AthleteTableStore {
  // variables for the table
  @observable athleteList: AthleteI[] = [];
  @observable currentPage: number = 0;
  @observable sizePage: number = 10;
  @observable totalPages: number = 2;

  // variables for the modals
  @observable showInputForm: boolean = false;
  @observable isDeleteFormOpened: boolean = false;
  @observable isGunsFormOpened: boolean = false;

  // variables for the input
  @observable inputAthlete: InputAthleteI = {
    firstName: "",
    lastName: "",
    fatherName: "",
    motherName: "",
    telephone: "",
    address: "",
    email: "",
    town: "",
    postalCode: "",
    amka: "",
    idNumber: "",
    association: "",
    licenseNumber: "",
    issuingAuthority: "",
    birthDate: new Date("1988-12-17T03:24:00"),
    issueDate: new Date("1995-12-17T03:24:00"),
    expirationDate: new Date("2000-12-17T03:24:00"),
    registrationDate: new Date("2010-12-17T03:24:00")
  };
  @observable selectedAthleteId?: number;
  @observable errorFieldsForInputAthlete = new Map<string, string>();

  // variables for gun table and input
  @observable guns: AthleteGunI[] = [];
  @observable inputGun: InputAthleteGunI = {
    caliber: "",
    armsNumber: "",
    athleteID: 0
  };
  @observable errorFieldsForInputGun = new Map<string, string>();

  @action
  changeSizePage = async (size: number) => {
    this.sizePage = size;
    await this.selectPageForAthletes(0);
  };

  @action
  selectPageForAthletes = async (page: number) => {
    const lmp = await getAthletesPaginated({
      page,
      size: this.sizePage
    });
    console.log("total pages", lmp.totalPages);
    this.athleteList = lmp.data;
    this.currentPage = lmp.page;
    this.totalPages = lmp.totalPages;
    this.sizePage = lmp.size;
  };

  @action
  showAddModal = () => {
    this.showInputForm = true;
  };

  @action
  showEditModal = (id: number) => {
    return async () => {
      this.showInputForm = true;
      const out = await getAthlete(id);
      if (!isErrorI(out)) {
        const ath = out as AthleteI;
        this.inputAthlete = ath;
        this.selectedAthleteId = ath.id;
      }
    };
  };

  @action
  deleteAthlete = async () => {
    if (this.selectedAthleteId) {
      await deleteAthlete(this.selectedAthleteId);
      await this.selectPageForAthletes(0);
      console.log("deleted athlete", this.selectedAthleteId);
      this.selectedAthleteId = undefined;
    } else {
      console.log("Error: this.selectedAthleteId is undefined");
    }
    this.isDeleteFormOpened = false;
  };

  @action
  selectToDeleteAthlete = (id: number) => {
    return () => {
      this.selectedAthleteId = id;
      this.isDeleteFormOpened = true;
    };
  };

  @action
  selectAthleteGuns = (id: number) => {
    return async () => {
      this.isGunsFormOpened = true;
      this.selectedAthleteId = id;
      this.guns = await getGunsByAthlete(id);
    };
  };

  @action
  closeModal = () => {
    this.showInputForm = false;
    this.isDeleteFormOpened = false;
    this.isGunsFormOpened = false;
    this.selectedAthleteId = undefined;
    this.errorFieldsForInputAthlete.clear();
    this.errorFieldsForInputGun.clear();
    this.inputAthlete = {
      firstName: "",
      lastName: "",
      fatherName: "",
      motherName: "",
      telephone: "",
      address: "",
      email: "",
      town: "",
      postalCode: "",
      amka: "",
      idNumber: "",
      association: "",
      licenseNumber: "",
      issuingAuthority: "",
      birthDate: new Date("1988-12-17T03:24:00"),
      issueDate: new Date("1995-12-17T03:24:00"),
      expirationDate: new Date("2000-12-17T03:24:00"),
      registrationDate: new Date("2010-12-17T03:24:00")
    };
    this.guns = [];
  };

  @action
  afterAddingAthlete = () => {
    this.selectPageForAthletes(0);
    this.showInputForm = false;
    this.errorFieldsForInputAthlete.clear();
    this.selectedAthleteId = undefined;
    this.inputAthlete = {
      firstName: "",
      lastName: "",
      fatherName: "",
      motherName: "",
      telephone: "",
      address: "",
      email: "",
      town: "",
      postalCode: "",
      amka: "",
      idNumber: "",
      association: "",
      licenseNumber: "",
      issuingAuthority: "",
      birthDate: new Date("1988-12-17T03:24:00"),
      issueDate: new Date("1995-12-17T03:24:00"),
      expirationDate: new Date("2000-12-17T03:24:00"),
      registrationDate: new Date("2010-12-17T03:24:00")
    };
  };

  @action
  addAthlete = async () => {
    const out = await addAthlete(this.inputAthlete);
    if (isErrorI(out)) {
      const value = out as ErrorI;
      if (value.fields.length > 0) {
        value.fields.forEach(v => {
          this.errorFieldsForInputAthlete.set(v.field, v.message);
        });
      }
    } else {
      console.log("Added", out);
      this.afterAddingAthlete();
    }
  };

  @action
  editAthlete = async () => {
    if (this.selectedAthleteId) {
      const out = await editAthlete(this.selectedAthleteId, this.inputAthlete);
      if (isErrorI(out)) {
        const value = out as ErrorI;
        if (value.fields.length > 0) {
          value.fields.forEach(v => {
            this.errorFieldsForInputAthlete.set(v.field, v.message);
          });
        }
      } else {
        console.log("Edit", out);
        this.afterAddingAthlete();
      }
    }
  };

  @action
  deleteGun = (id: number) => {
    return async () => {
      const out = await deleteAthleteGun(id);
      if (isErrorI(out)) {
        console.log("Error: ", out);
      } else {
        if (this.selectedAthleteId) {
          this.guns = await getGunsByAthlete(this.selectedAthleteId);
        }
      }
    };
  };

  @action
  addGun = async () => {
    if (this.selectedAthleteId) {
      this.inputGun.athleteID = this.selectedAthleteId;
      const out = await addAthleteGun(this.inputGun);
      if (isErrorI(out)) {
        const errs = out as ErrorI;
        errs.fields.forEach(v => {
          this.errorFieldsForInputGun.set(v.field, v.message);
        });
      } else {
        this.guns = await getGunsByAthlete(this.selectedAthleteId);
        this.inputGun.armsNumber = "";
        this.inputGun.caliber = "";
        this.errorFieldsForInputGun.clear();
      }
    }
  };
}
