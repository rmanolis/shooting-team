import { observable, action } from "mobx";
import {
  ShootingRangeI,
  InputShootingRangeI
} from "../../../main/ipc-server/models/shooting-range.interface";
import {
  ErrorI,
  isErrorI
} from "../../../main/ipc-server/models/common.interface";
import { AthleteI } from "../../../main/ipc-server/models/athlete.interface";
import {
  getShootingRangesPaginatedByDate,
  getShootingRange,
  addInputShootingRange,
  editInputShootingRange,
  deleteShootingRange
} from "../callers/shooting-range.caller";
import { getAthlete } from "../callers/athlete.caller";
import { DateEntityI } from "../../../main/ipc-server/models/date.interface";
import {
  getTodayDate,
  getDateEntitiesPaginated
} from "../callers/date-entity.caller";

export class ShootingRangeTableStore {
  // attributes for the form to select dates
  @observable shootingRangeDates: DateEntityI[] = [];
  @observable selectedShootingRangeDate?: DateEntityI = undefined;

  // attributes for the table
  @observable shootingRanges: ShootingRangeI[] = [];

  // attributes for the pagination
  @observable currentPage: number = 0;
  @observable sizePage: number = 10;
  @observable totalPages: number = 2;

  // attributes for the form to add or edit a shooting range
  @observable selectedAthleteForInput?: AthleteI = undefined;
  @observable input: InputShootingRangeI = {
    athleteID: 0,
    dateEntityID: 0,
    issuingAuthority: "",
    caliber: "",
    quantity: 0,
    gunNumber: "",
    warehouseQuantity: 0,
    isContest: false,
    isTraining: false
  };
  @observable inputErrorFields = new Map<string, string>();

  // attributes to show the forms
  @observable isSelectDateFormOpened: boolean = false;
  @observable isInputFormOpened: boolean = false;
  @observable isDeleteFormOpened: boolean = false;
  @observable selectedShootingRangeForEditing?: number = undefined;

  @action
  async selectPageForShootingRanges(page: number) {
    const srd = this.selectedShootingRangeDate;
    if (srd) {
      const lmp = await getShootingRangesPaginatedByDate(srd.id, {
        page,
        size: this.sizePage
      });
      this.shootingRanges = lmp.data;
      this.currentPage = lmp.page;
      this.totalPages = lmp.totalPages;
      this.sizePage = lmp.size;
    }
  }

  @action
  showDeleteForm = (id: number) => {
    return () => {
      this.selectedShootingRangeForEditing = id;
      this.isDeleteFormOpened = true;
    };
  };

  @action
  deleteShootingRange = async () => {
    const selected = this.selectedShootingRangeForEditing;
    if (selected) {
      await deleteShootingRange(selected);
      await this.selectPageForShootingRanges(0);
      console.log("deleted shooting range ", selected);
      this.selectedShootingRangeForEditing = undefined;
    } else {
      console.log("Error: this.selectedShootingRangeForEditing is undefined");
    }
    this.isDeleteFormOpened = false;
  };

  @action
  changeSizePage = async (size: number) => {
    this.sizePage = size;
    await this.selectPageForShootingRanges(0);
  };

  @action
  closeModal = () => {
    this.isSelectDateFormOpened = false;
    this.isInputFormOpened = false;
    this.isDeleteFormOpened = false;
    this.selectedShootingRangeForEditing = undefined;
  };

  @action
  afterDateSelectionFromDateForm = (srd: DateEntityI) => {
    this.isSelectDateFormOpened = false;
    this.selectedShootingRangeDate = srd;
    this.selectedShootingRangeForEditing = undefined;
    this.selectPageForShootingRanges(0);
  };

  @action
  afterAddingShootingRangeFromInputForm = () => {
    this.isInputFormOpened = false;
    this.selectedShootingRangeForEditing = undefined;
    this.selectPageForShootingRanges(0);
  };

  @action
  showFormToSelectDateModal = () => {
    this.isSelectDateFormOpened = true;
    this.shootingRangeDates = [];
    this.pushShootingRangeDates();
  };

  @action
  showInputForm = (id?: number) => {
    return async () => {
      if (id) {
        const sr = await getShootingRange(id);
        this.input = {
          athleteID: sr.athleteID,
          dateEntityID: sr.dateEntityID,
          issuingAuthority: sr.issuingAuthority,
          caliber: sr.caliber,
          quantity: sr.quantity,
          gunNumber: sr.gunNumber,
          warehouseQuantity: sr.warehouseQuantity,
          isContest: sr.isContest,
          isTraining: sr.isTraining
        };
        const athlete = await getAthlete(sr.athleteID);
        if (isErrorI(athlete)) {
          console.log("Error from getAthlete: ", athlete);
        } else {
          const ath = athlete as AthleteI;
          this.selectedAthleteForInput = ath;
          this.isInputFormOpened = true;
          this.selectedShootingRangeForEditing = id;
        }
      } else {
        this.selectedShootingRangeForEditing = undefined;
        this.isInputFormOpened = true;
      }
    };
  };

  @action
  async setShootingRangeDateFromToday() {
    const today = await getTodayDate();
    if (isErrorI(today)) {
      console.log("Error setShootingRangeDateFromToday: ", today);
    } else {
      const td = today as DateEntityI;
      this.selectedShootingRangeDate = td;
    }
  }

  @action
  pushShootingRangeDates = async (): Promise<boolean> => {
    let lastId: number | undefined = undefined;
    if (this.shootingRangeDates.length > 0) {
      lastId = this.shootingRangeDates[this.shootingRangeDates.length - 1].id;
    }
    const dates = await getDateEntitiesPaginated(5, lastId);
    if (dates.length > 0) {
      this.shootingRangeDates.push(...dates);
      return true;
    } else {
      return false;
    }
  };

  @action
  emptyInput() {
    this.input = {
      athleteID: 0,
      dateEntityID: 0,
      issuingAuthority: "",
      caliber: "",
      quantity: 0,
      gunNumber: "",
      warehouseQuantity: 0,
      isContest: false,
      isTraining: false
    };
  }

  @action
  async addInputShootingRange() {
    this.input.dateEntityID = this.selectedShootingRangeDate!.id;
    const out = await addInputShootingRange(this.input);
    if (isErrorI(out)) {
      const value = out as ErrorI;
      if (value.fields.length > 0) {
        value.fields.forEach(v => {
          this.inputErrorFields.set(v.field, v.message);
        });
      }
    } else {
      this.afterAddingShootingRangeFromInputForm();
      this.emptyInput();
      this.selectedAthleteForInput = undefined;
    }
  }

  @action
  async editInputShootingRange() {
    this.input.dateEntityID = this.selectedShootingRangeDate!.id;
    const out = await editInputShootingRange(
      this.selectedShootingRangeForEditing!,
      this.input
    );
    if (isErrorI(out)) {
      const value = out as ErrorI;
      if (value.fields.length > 0) {
        value.fields.forEach(v => {
          this.inputErrorFields.set(v.field, v.message);
        });
      }
    } else {
      this.afterAddingShootingRangeFromInputForm();
      this.emptyInput();
      this.selectedAthleteForInput = undefined;
    }
  }
}
