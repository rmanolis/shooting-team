import * as React from 'react';
import { Component } from 'react';
import { NavigationComponent } from '../components/navigation.component';

interface HomeProps {}
export class HomePage extends Component<HomeProps> {
  selectMember = (id: number) => {
    console.log('selected member ', id);
  };
  render() {
    return (
      <div>
        <NavigationComponent selectedLink={'/'} />
        <div>Home</div>
      </div>
    );
  }
}
