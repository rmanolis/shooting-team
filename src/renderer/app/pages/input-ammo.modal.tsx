import * as React from "react";
import { observer, inject } from "mobx-react";
import { AmmoTableStore } from "../stores/ammo-table.store";
import {
  AmmoTranslations,
  AmmoKeys
} from "../../../main/ipc-server/models/ammo.interface";
import { AthleteI } from "../../../main/ipc-server/models/athlete.interface";
import { SelectAthleteComponent } from "../components/select-athlete.component";
import { Modal, Form, Row, Col, Button } from "react-bootstrap";
import { DateEntityI } from "../../../main/ipc-server/models/date.interface";

interface InputAmmoModalProps {
  ammoTableStore?: AmmoTableStore;
  ammoDate?: DateEntityI;
  selectedIDForEditing?: number;
  showModal: boolean;
  closeModal: () => void;
}

@inject("ammoTableStore")
@observer
export class InputAmmoModal extends React.Component<InputAmmoModalProps> {
  handleChange = (key: string) => {
    return ({
      currentTarget: { value }
    }: React.SyntheticEvent<HTMLInputElement>) => {
      if (typeof this.props.ammoTableStore!.input[key] === "boolean") {
        const defValue = this.props.ammoTableStore!.input[key];
        this.props.ammoTableStore!.input[key] = !defValue;
      } else if (typeof this.props.ammoTableStore!.input[key] === "number") {
        this.props.ammoTableStore!.input[key] = parseInt(value);
      } else {
        this.props.ammoTableStore!.input[key] = value;
      }
    };
  };

  selectAthlete = (ath: AthleteI) => {
    this.props.ammoTableStore!.selectedAthleteForInput = ath;
    this.props.ammoTableStore!.input.athleteID = ath.id;
  };

  closeModal = () => {
    this.props.closeModal();
    this.props.ammoTableStore!.emptyInput();
    this.props.ammoTableStore!.selectedAthleteForInput = undefined;
  };

  handleSubmit = () => {
    if (this.props.selectedIDForEditing) {
      this.props.ammoTableStore!.editInputAmmo();
    } else {
      this.props.ammoTableStore!.addInputAmmo();
    }
  };

  inputForm(): JSX.Element {
    const input = this.props.ammoTableStore!.input;
    const ath = this.props.ammoTableStore!.selectedAthleteForInput;
    return (
      <div>
        <Form>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>{AmmoTranslations[AmmoKeys.AthleteID]}:</Form.Label>
            </Col>
            <Col sm={5}>
              <Form.Text>{ath!.firstName + " " + ath!.lastName}</Form.Text>
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {AmmoTranslations[AmmoKeys.DateEntityID]}:
              </Form.Label>
            </Col>
            <Col sm={5}>
              <Form.Text>{this.props.ammoDate!.forDate}</Form.Text>
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>{AmmoTranslations[AmmoKeys.Caliber]}:</Form.Label>
            </Col>
            <Col sm={5}>
              <input
                value={input[AmmoKeys.Caliber]}
                onChange={this.handleChange(AmmoKeys.Caliber)}
              />
              {this.props.ammoTableStore!.inputErrorFields.has(
                AmmoKeys.Caliber
              ) && (
                <div>
                  {this.props.ammoTableStore!.inputErrorFields.get(
                    AmmoKeys.Caliber
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>{AmmoTranslations[AmmoKeys.Location]}:</Form.Label>
            </Col>
            <Col sm={5}>
              <input
                value={input[AmmoKeys.Location]}
                onChange={this.handleChange(AmmoKeys.Location)}
              />
              {this.props.ammoTableStore!.inputErrorFields.has(
                AmmoKeys.Location
              ) && (
                <div>
                  {this.props.ammoTableStore!.inputErrorFields.get(
                    AmmoKeys.Location
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>{AmmoTranslations[AmmoKeys.Quantity]}:</Form.Label>
            </Col>
            <Col sm={5}>
              <input
                value={input[AmmoKeys.Quantity]}
                onChange={this.handleChange(AmmoKeys.Quantity)}
              />
              {this.props.ammoTableStore!.inputErrorFields.has(
                AmmoKeys.Quantity
              ) && (
                <div>
                  {this.props.ammoTableStore!.inputErrorFields.get(
                    AmmoKeys.Quantity
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Button variant="primary" onClick={this.handleSubmit}>
              Submit
            </Button>
            <Button variant="secondary" onClick={this.closeModal}>
              Close
            </Button>
          </Form.Group>
        </Form>
      </div>
    );
  }

  render() {
    const menu =
      this.props.ammoTableStore!.selectedAthleteForInput === undefined ? (
        <SelectAthleteComponent selectAthlete={this.selectAthlete} />
      ) : (
        this.inputForm()
      );
    return (
      <div>
        <Modal show={this.props.showModal} size="lg" onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>Νέα είσοδο Φυσιγγίων</Modal.Title>
          </Modal.Header>
          <Modal.Body>{menu}</Modal.Body>
        </Modal>
      </div>
    );
  }
}
