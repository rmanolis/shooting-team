import * as React from "react";
import { Component } from "react";
import { NavigationComponent } from "../components/navigation.component";
import { AmmoTableStore } from "../stores/ammo-table.store";
import { inject, observer } from "mobx-react";
import { PaginationComponent } from "../components/pagination.component";
import { SelectDateComponent } from "../components/select-date.component";
import { InputAmmoModal } from "./input-ammo.modal";
import { Form, Button, Col, Row } from "react-bootstrap";
import {
  AmmoKeys,
  AmmoTranslations
} from "../../../main/ipc-server/models/ammo.interface";
import { Column, Cell, Table } from "@blueprintjs/table";
import { DeleteComponent } from "../components/delete.component";

interface AmmoTableProps {
  ammoTableStore?: AmmoTableStore;
}

@inject("ammoTableStore")
@observer
export class AmmoTablePage extends Component<AmmoTableProps> {
  componentWillMount() {
    this.props
      .ammoTableStore!.setAmmoDateFromToday()
      .then(() => this.props.ammoTableStore!.selectPageForAmmos(0));
  }

  rowNameRender = (key: string) => {
    return (rowIndex: number) => {
      const list = this.props.ammoTableStore!.ammoList;
      if (key === "id") {
        const id = list[rowIndex][key];
        return (
          <Cell>
            <React.Fragment>
              <button onClick={this.props.ammoTableStore!.showInputForm(id)}>
                Edit
              </button>
              <button onClick={this.props.ammoTableStore!.showDeleteForm(id)}>
                Del
              </button>
            </React.Fragment>
          </Cell>
        );
      }
      const value = list[rowIndex][key];
      if (typeof value === "boolean") {
        const greekBoolean = value ? "Ναι" : "Όχι";
        return <Cell>{greekBoolean} </Cell>;
      } else {
        return <Cell>{list[rowIndex][key]} </Cell>;
      }
    };
  };

  render() {
    const sdt = this.props.ammoTableStore!.selectedAmmoDate;
    const selectedDate = sdt
      ? new Date(sdt.forDate).toLocaleDateString("en-US")
      : "";
    const listSize = this.props.ammoTableStore!.ammoList.length;
    const items = Object.keys(AmmoKeys).map((v, index) => {
      return (
        <Column
          key={index}
          name={AmmoTranslations[AmmoKeys[v]]}
          cellRenderer={this.rowNameRender(AmmoKeys[v])}
        />
      );
    });
    return (
      <div>
        <NavigationComponent selectedLink={"/ammoTable"} />
        <div>
          <Form>
            <Form.Group as={Row}>
              <Col sm="2">
                <Form.Label>Επιλεγμένη ημερομηνία</Form.Label>
              </Col>
              <Col sm="2">
                <Form.Text>{selectedDate}</Form.Text>
              </Col>
              <Col>
                {
                  <Button
                    variant="primary"
                    onClick={
                      this.props.ammoTableStore!.showFormToSelectDateModal
                    }
                  >
                    Επιλέξτε
                  </Button>
                }
              </Col>
            </Form.Group>
            <Form.Group>
              <Button onClick={this.props.ammoTableStore!.showInputForm()}>
                Εισαγωγή
              </Button>
            </Form.Group>
          </Form>
        </div>
        <div>
          <Table numRows={listSize}>{items}</Table>
        </div>
        <PaginationComponent
          changeSizePage={this.props.ammoTableStore!.changeSizePage}
          sizePage={this.props.ammoTableStore!.sizePage}
          totalNumberPages={this.props.ammoTableStore!.totalPages}
          currentPage={this.props.ammoTableStore!.currentPage}
          selectPage={this.props.ammoTableStore!.selectPageForAmmos}
        />
        <SelectDateComponent
          shootingRangeDates={this.props.ammoTableStore!.ammoDates}
          pushShootingRangeDates={this.props.ammoTableStore!.pushAmmoDates}
          showModal={this.props.ammoTableStore!.isSelectDateFormOpened}
          closeModal={this.props.ammoTableStore!.closeModal}
          afterSelection={
            this.props.ammoTableStore!.afterDateSelectionFromDateForm
          }
        />
        <DeleteComponent
          delete={this.props.ammoTableStore!.deleteAmmo}
          closeModal={this.props.ammoTableStore!.closeModal}
          showModal={this.props.ammoTableStore!.isDeleteFormOpened}
        />
        <InputAmmoModal
          ammoDate={this.props.ammoTableStore!.selectedAmmoDate}
          selectedIDForEditing={
            this.props.ammoTableStore!.selectedAmmoForEditing
          }
          showModal={this.props.ammoTableStore!.isInputFormOpened}
          closeModal={this.props.ammoTableStore!.closeModal}
        />
      </div>
    );
  }
}
