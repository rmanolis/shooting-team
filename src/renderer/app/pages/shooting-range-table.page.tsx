import * as React from "react";
import { Component } from "react";
import { NavigationComponent } from "../components/navigation.component";
import { ShootingRangeTableStore } from "../stores/shooting-range-table.store";
import { inject, observer } from "mobx-react";
import {
  ShootingRangeKeys,
  ShootingRangeTranslations
} from "../../../main/ipc-server/models/shooting-range.interface";
import { Form, Row, Col, Button } from "react-bootstrap";
import { SelectDateComponent } from "../components/select-date.component";
import { InputShootingRangeModal } from "./input-shooting-range.modal";
import { Cell, Table, Column } from "@blueprintjs/table";
import { PaginationComponent } from "../components/pagination.component";
import { DeleteComponent } from "../components/delete.component";

interface ShootingRangeTableProps {
  shootingRangeTableStore?: ShootingRangeTableStore;
}

@inject("shootingRangeTableStore")
@observer
export class ShootingRangeTablePage extends Component<ShootingRangeTableProps> {
  componentWillMount() {
    this.props
      .shootingRangeTableStore!.setShootingRangeDateFromToday()
      .then(() =>
        this.props.shootingRangeTableStore!.selectPageForShootingRanges(0)
      );
  }

  rowNameRender = (key: string) => {
    const store = this.props.shootingRangeTableStore;
    return (rowIndex: number) => {
      const list = this.props.shootingRangeTableStore!.shootingRanges;
      if (key === "id") {
        const id = list[rowIndex][key];
        return (
          <Cell>
            <React.Fragment>
              <button onClick={store!.showInputForm(id)}>Edit</button>
              <button onClick={store!.showDeleteForm(id)}>Del</button>
            </React.Fragment>
          </Cell>
        );
      }
      const value = list[rowIndex][key];
      if (typeof value === "boolean") {
        const greekBoolean = value ? "Ναι" : "Όχι";
        return <Cell>{greekBoolean} </Cell>;
      } else {
        return <Cell>{list[rowIndex][key]} </Cell>;
      }
    };
  };

  render() {
    const sdt = this.props.shootingRangeTableStore!.selectedShootingRangeDate;
    let selectedDate = "";
    if (sdt) {
      selectedDate = new Date(sdt.forDate).toLocaleDateString("en-US");
    }
    const listSize = this.props.shootingRangeTableStore!.shootingRanges.length;
    const items = Object.keys(ShootingRangeKeys).map((v, index) => {
      return (
        <Column
          key={index}
          name={ShootingRangeTranslations[ShootingRangeKeys[v]]}
          cellRenderer={this.rowNameRender(ShootingRangeKeys[v])}
        />
      );
    });
    return (
      <div>
        <NavigationComponent selectedLink={"/shootingRangeTable"} />
        <div>
          <Form>
            <Form.Group as={Row}>
              <Col sm="2">
                <Form.Label>Επιλεγμένη ημερομηνία</Form.Label>
              </Col>
              <Col sm="2">
                <Form.Text>{selectedDate}</Form.Text>
              </Col>
              <Col>
                {
                  <Button
                    variant="primary"
                    onClick={
                      this.props.shootingRangeTableStore!
                        .showFormToSelectDateModal
                    }
                  >
                    Επιλέξτε
                  </Button>
                }
              </Col>
            </Form.Group>
            <Form.Group>
              <Button
                onClick={this.props.shootingRangeTableStore!.showInputForm()}
              >
                Εισαγωγή
              </Button>
            </Form.Group>
          </Form>
        </div>
        <div>
          <Table numRows={listSize}>{items}</Table>
        </div>
        <PaginationComponent
          changeSizePage={this.props.shootingRangeTableStore!.changeSizePage}
          sizePage={this.props.shootingRangeTableStore!.sizePage}
          totalNumberPages={this.props.shootingRangeTableStore!.totalPages}
          currentPage={this.props.shootingRangeTableStore!.currentPage}
          selectPage={
            this.props.shootingRangeTableStore!.selectPageForShootingRanges
          }
        />
        <SelectDateComponent
          shootingRangeDates={
            this.props.shootingRangeTableStore!.shootingRangeDates
          }
          pushShootingRangeDates={
            this.props.shootingRangeTableStore!.pushShootingRangeDates
          }
          showModal={this.props.shootingRangeTableStore!.isSelectDateFormOpened}
          closeModal={this.props.shootingRangeTableStore!.closeModal}
          afterSelection={
            this.props.shootingRangeTableStore!.afterDateSelectionFromDateForm
          }
        />
        <DeleteComponent
          delete={this.props.shootingRangeTableStore!.deleteShootingRange}
          closeModal={this.props.shootingRangeTableStore!.closeModal}
          showModal={this.props.shootingRangeTableStore!.isDeleteFormOpened}
        />
        <InputShootingRangeModal
          shootingRangeDate={sdt}
          selectedIDForEditing={
            this.props.shootingRangeTableStore!.selectedShootingRangeForEditing
          }
          showModal={this.props.shootingRangeTableStore!.isInputFormOpened}
          closeModal={this.props.shootingRangeTableStore!.closeModal}
        />
      </div>
    );
  }
}
