import * as React from 'react';
import { Component } from 'react';
import { AthleteTableStore } from '../stores/athlete-table.store';
import { observer, inject } from 'mobx-react';
import { Modal, Form, Row, Col } from 'react-bootstrap';
import {
  AthleteGunKeys,
  AthleteGunTranslations,
} from '../../../main/ipc-server/models/athlete-gun.interface';
import { Column, Cell, Table } from '@blueprintjs/table';
import { Button } from '@blueprintjs/core';

interface AthleteGunTableModalProps {
  athleteTableStore?: AthleteTableStore;
  showModal: boolean;
  closeModal: () => void;
}

@inject('athleteTableStore')
@observer
export class AthleteGunTableModal extends Component<AthleteGunTableModalProps> {
  handleChange = (key: string) => {
    return ({
      currentTarget: { value },
    }: React.SyntheticEvent<HTMLInputElement>) => {
      this.props.athleteTableStore!.inputGun[key] = value;
    };
  };

  rowNameRender = (key: string) => {
    return (rowIndex: number) => {
      const { guns, deleteGun } = this.props.athleteTableStore!;
      if (key === 'Delete') {
        return (
          <Cell>
            <React.Fragment>
              <Button onClick={deleteGun(guns[rowIndex]['id'])}>x</Button>
            </React.Fragment>
          </Cell>
        );
      } else {
        return <Cell>{guns[rowIndex][key]} </Cell>;
      }
    };
  };

  render() {
    const listSize = this.props.athleteTableStore!.guns.length;
    const items = Object.keys(AthleteGunKeys).map((v, index) => {
      return (
        <Column
          key={index}
          name={AthleteGunTranslations[AthleteGunKeys[v]]}
          cellRenderer={this.rowNameRender(AthleteGunKeys[v])}
        />
      );
    });
    items.push(
      <Column
        key={items.length}
        name="Delete"
        cellRenderer={this.rowNameRender('Delete')}
      />,
    );
    const input = this.props.athleteTableStore!.inputGun;
    const errorFields = this.props.athleteTableStore!.errorFieldsForInputGun;
    const addGun = this.props.athleteTableStore!.addGun;
    return (
      <div>
        <Modal
          show={this.props.showModal}
          size="lg"
          onHide={this.props.closeModal}
        >
          <Modal.Header closeButton>
            <Modal.Title>Όπλα</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group as={Row}>
                <Form.Label column>
                  {AthleteGunTranslations[AthleteGunKeys.ArmsNumber]}
                </Form.Label>
                <Col>
                  <input
                    value={input[AthleteGunKeys.ArmsNumber]}
                    onChange={this.handleChange(AthleteGunKeys.ArmsNumber)}
                  />
                  {errorFields.has(AthleteGunKeys.ArmsNumber) && (
                    <div>{errorFields.get(AthleteGunKeys.ArmsNumber)}</div>
                  )}
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column>
                  {AthleteGunTranslations[AthleteGunKeys.Caliber]}
                </Form.Label>
                <Col>
                  <input
                    value={input[AthleteGunKeys.Caliber]}
                    onChange={this.handleChange(AthleteGunKeys.Caliber)}
                  />
                  {errorFields.has(AthleteGunKeys.Caliber) && (
                    <div>{errorFields.get(AthleteGunKeys.Caliber)}</div>
                  )}
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Button onClick={addGun}>Add</Button>
              </Form.Group>
            </Form>
            <div>
              <Table numRows={listSize}>{items}</Table>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
