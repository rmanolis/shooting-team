import * as React from "react";
import { observer, inject } from "mobx-react";
import { ShootingRangeTableStore } from "../stores/shooting-range-table.store";
import { Modal, Form, Row, Col, Button } from "react-bootstrap";
import { SelectAthleteComponent } from "../components/select-athlete.component";
import { AthleteI } from "../../../main/ipc-server/models/athlete.interface";
import {
  ShootingRangeTranslations,
  ShootingRangeKeys
} from "../../../main/ipc-server/models/shooting-range.interface";
import { DateEntityI } from "../../../main/ipc-server/models/date.interface";

interface InputShootingRangeModalProps {
  shootingRangeTableStore?: ShootingRangeTableStore;
  shootingRangeDate?: DateEntityI;
  selectedIDForEditing?: number;
  showModal: boolean;
  closeModal: () => void;
}

@inject("shootingRangeTableStore")
@observer
export class InputShootingRangeModal extends React.Component<
  InputShootingRangeModalProps
> {
  handleChange = (key: string) => {
    return ({
      currentTarget: { value }
    }: React.SyntheticEvent<HTMLInputElement>) => {
      if (typeof this.props.shootingRangeTableStore!.input[key] === "boolean") {
        const defValue = this.props.shootingRangeTableStore!.input[key];
        this.props.shootingRangeTableStore!.input[key] = !defValue;
      } else if (
        typeof this.props.shootingRangeTableStore!.input[key] === "number"
      ) {
        this.props.shootingRangeTableStore!.input[key] = parseInt(value);
      } else {
        this.props.shootingRangeTableStore!.input[key] = value;
      }
    };
  };

  selectAthlete = (ath: AthleteI) => {
    this.props.shootingRangeTableStore!.selectedAthleteForInput = ath;
    this.props.shootingRangeTableStore!.input.athleteID = ath.id;
  };

  closeModal = () => {
    this.props.closeModal();
    this.props.shootingRangeTableStore!.emptyInput();
    this.props.shootingRangeTableStore!.selectedAthleteForInput = undefined;
  };

  handleSubmit = () => {
    if (this.props.selectedIDForEditing) {
      this.props.shootingRangeTableStore!.editInputShootingRange();
    } else {
      this.props.shootingRangeTableStore!.addInputShootingRange();
    }
  };

  inputForm(): JSX.Element {
    const input = this.props.shootingRangeTableStore!.input;
    const ath = this.props.shootingRangeTableStore!.selectedAthleteForInput;
    return (
      <div>
        <Form>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {ShootingRangeTranslations[ShootingRangeKeys.AthleteID]}:
              </Form.Label>
            </Col>
            <Col sm={5}>
              <Form.Text>{ath!.firstName + " " + ath!.lastName}</Form.Text>
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {ShootingRangeTranslations[ShootingRangeKeys.DateEntityID]}:
              </Form.Label>
            </Col>
            <Col sm={5}>
              <Form.Text>{this.props.shootingRangeDate!.forDate}</Form.Text>
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {ShootingRangeTranslations[ShootingRangeKeys.Caliber]}:
              </Form.Label>
            </Col>
            <Col sm={5}>
              <input
                value={input[ShootingRangeKeys.Caliber]}
                onChange={this.handleChange(ShootingRangeKeys.Caliber)}
              />
              {this.props.shootingRangeTableStore!.inputErrorFields.has(
                ShootingRangeKeys.Caliber
              ) && (
                <div>
                  {this.props.shootingRangeTableStore!.inputErrorFields.get(
                    ShootingRangeKeys.Caliber
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {ShootingRangeTranslations[ShootingRangeKeys.IssuingAuthority]}:
              </Form.Label>
            </Col>
            <Col sm={5}>
              <input
                value={input[ShootingRangeKeys.IssuingAuthority]}
                onChange={this.handleChange(ShootingRangeKeys.IssuingAuthority)}
              />
              {this.props.shootingRangeTableStore!.inputErrorFields.has(
                ShootingRangeKeys.IssuingAuthority
              ) && (
                <div>
                  {this.props.shootingRangeTableStore!.inputErrorFields.get(
                    ShootingRangeKeys.IssuingAuthority
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {ShootingRangeTranslations[ShootingRangeKeys.Quantity]}:
              </Form.Label>
            </Col>
            <Col sm={5}>
              <input
                value={input[ShootingRangeKeys.Quantity]}
                onChange={this.handleChange(ShootingRangeKeys.Quantity)}
              />
              {this.props.shootingRangeTableStore!.inputErrorFields.has(
                ShootingRangeKeys.Quantity
              ) && (
                <div>
                  {this.props.shootingRangeTableStore!.inputErrorFields.get(
                    ShootingRangeKeys.Quantity
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {ShootingRangeTranslations[ShootingRangeKeys.WarehouseQuantity]}
                :
              </Form.Label>
            </Col>
            <Col sm={5}>
              <input
                value={input[ShootingRangeKeys.WarehouseQuantity]}
                onChange={this.handleChange(
                  ShootingRangeKeys.WarehouseQuantity
                )}
              />
              {this.props.shootingRangeTableStore!.inputErrorFields.has(
                ShootingRangeKeys.WarehouseQuantity
              ) && (
                <div>
                  {this.props.shootingRangeTableStore!.inputErrorFields.get(
                    ShootingRangeKeys.WarehouseQuantity
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {ShootingRangeTranslations[ShootingRangeKeys.GunNumber]}:
              </Form.Label>
            </Col>
            <Col sm={5}>
              <input
                value={input[ShootingRangeKeys.GunNumber]}
                onChange={this.handleChange(ShootingRangeKeys.GunNumber)}
              />
              {this.props.shootingRangeTableStore!.inputErrorFields.has(
                ShootingRangeKeys.GunNumber
              ) && (
                <div>
                  {this.props.shootingRangeTableStore!.inputErrorFields.get(
                    ShootingRangeKeys.GunNumber
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {ShootingRangeTranslations[ShootingRangeKeys.IsContest]}:
              </Form.Label>
            </Col>
            <Col sm={5}>
              <input
                type="checkbox"
                checked={input[ShootingRangeKeys.IsContest]}
                onChange={this.handleChange(ShootingRangeKeys.IsContest)}
              />
              {this.props.shootingRangeTableStore!.inputErrorFields.has(
                ShootingRangeKeys.IsContest
              ) && (
                <div>
                  {this.props.shootingRangeTableStore!.inputErrorFields.get(
                    ShootingRangeKeys.IsContest
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={2}>
              <Form.Label>
                {ShootingRangeTranslations[ShootingRangeKeys.IsTraining]}:
              </Form.Label>
            </Col>
            <Col sm={5}>
              <input
                type="checkbox"
                checked={input[ShootingRangeKeys.IsTraining]}
                onChange={this.handleChange(ShootingRangeKeys.IsTraining)}
              />
              {this.props.shootingRangeTableStore!.inputErrorFields.has(
                ShootingRangeKeys.IsTraining
              ) && (
                <div>
                  {this.props.shootingRangeTableStore!.inputErrorFields.get(
                    ShootingRangeKeys.IsTraining
                  )}
                </div>
              )}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Button variant="primary" onClick={this.handleSubmit}>
              Submit
            </Button>
            <Button variant="secondary" onClick={this.closeModal}>
              Close
            </Button>
          </Form.Group>
        </Form>
      </div>
    );
  }

  render() {
    const menu =
      this.props.shootingRangeTableStore!.selectedAthleteForInput ===
      undefined ? (
        <SelectAthleteComponent selectAthlete={this.selectAthlete} />
      ) : (
        this.inputForm()
      );
    return (
      <div>
        <Modal show={this.props.showModal} size="lg" onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>Νέα είσοδο Σκοπευτηρίου</Modal.Title>
          </Modal.Header>
          <Modal.Body>{menu}</Modal.Body>
        </Modal>
      </div>
    );
  }
}
