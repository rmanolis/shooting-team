import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { Button, Modal, Col, Form, Row } from 'react-bootstrap';
import {
  AthleteTranslations,
  AthleteKeys,
} from '../../../main/ipc-server/models/athlete.interface';
import { AthleteTableStore } from '../stores/athlete-table.store';
import DatePicker from 'react-datepicker';

interface InputAthleteModalProps {
  athleteTableStore?: AthleteTableStore;
  showModal: boolean;
  closeModal: () => void;
  afterAdd: () => void;
}

@inject('athleteTableStore')
@observer
export class InputAthleteModal extends React.Component<InputAthleteModalProps> {
 

  handleChange = (key: string) => {
    return ({
      currentTarget: { value },
    }: React.SyntheticEvent<HTMLInputElement>) => {
      this.props.athleteTableStore!.inputAthlete[key] = value;
    };
  };

  handleDateChange = (key: string) => {
    return (value: Date | null) => {
      this.props.athleteTableStore!.inputAthlete[key] = value;
    };
  };

  handleSubmit = () => {
    if (this.props.athleteTableStore!.selectedAthleteId) {
      this.props.athleteTableStore!.editAthlete();
    } else {
      this.props.athleteTableStore!.addAthlete();
    }
  };

  render() {
    const input = this.props.athleteTableStore!.inputAthlete;
    const errorFields = this.props.athleteTableStore!
      .errorFieldsForInputAthlete;
    return (
      <div>
        <Modal
          show={this.props.showModal}
          size="lg"
          onHide={this.props.closeModal}
        >
          <Modal.Header closeButton>
            <Modal.Title>Νέος Αθλητής</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Row>
                <Col>
                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.FirstName]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.FirstName]}
                        onChange={this.handleChange(AthleteKeys.FirstName)}
                      />
                      {errorFields.has(AthleteKeys.FirstName) && (
                        <div>{errorFields.get(AthleteKeys.FirstName)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.LastName]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.LastName]}
                        onChange={this.handleChange(AthleteKeys.LastName)}
                      />
                      {errorFields.has(AthleteKeys.LastName) && (
                        <div>{errorFields.get(AthleteKeys.LastName)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.FatherName]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.FatherName]}
                        onChange={this.handleChange(AthleteKeys.FatherName)}
                      />
                      {errorFields.has(AthleteKeys.FatherName) && (
                        <div>{errorFields.get(AthleteKeys.FatherName)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.MotherName]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.MotherName]}
                        onChange={this.handleChange(AthleteKeys.MotherName)}
                      />
                      {errorFields.has(AthleteKeys.MotherName) && (
                        <div>{errorFields.get(AthleteKeys.MotherName)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.Telephone]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.Telephone]}
                        onChange={this.handleChange(AthleteKeys.Telephone)}
                      />
                      {errorFields.has(AthleteKeys.Telephone) && (
                        <div>{errorFields.get(AthleteKeys.Telephone)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      <label>{AthleteTranslations[AthleteKeys.Address]}</label>
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.Address]}
                        onChange={this.handleChange(AthleteKeys.Address)}
                      />
                      {errorFields.has(AthleteKeys.Address) && (
                        <div>{errorFields.get(AthleteKeys.Address)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.Email]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.Email]}
                        onChange={this.handleChange(AthleteKeys.Email)}
                      />
                      {errorFields.has(AthleteKeys.Email) && (
                        <div>{errorFields.get(AthleteKeys.Email)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.Town]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.Town]}
                        onChange={this.handleChange(AthleteKeys.Town)}
                      />
                      {errorFields.has(AthleteKeys.Town) && (
                        <div>{errorFields.get(AthleteKeys.Town)}</div>
                      )}
                    </Col>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.BirthDate]}
                    </Form.Label>
                    <Col>
                      <DatePicker
                        selected={new Date(input[AthleteKeys.BirthDate])}
                        onChange={this.handleDateChange(AthleteKeys.BirthDate)}
                      />
                      {errorFields.has(AthleteKeys.BirthDate) && (
                        <div>{errorFields.get(AthleteKeys.BirthDate)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.IssueDate]}
                    </Form.Label>
                    <Col>
                      <DatePicker
                        selected={new Date(input[AthleteKeys.IssueDate])}
                        onChange={this.handleDateChange(AthleteKeys.IssueDate)}
                      />
                      {errorFields.has(AthleteKeys.IssueDate) && (
                        <div>{errorFields.get(AthleteKeys.IssueDate)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.ExpirationDate]}
                    </Form.Label>
                    <Col>
                      <DatePicker
                        selected={new Date(input[AthleteKeys.ExpirationDate])}
                        onChange={this.handleDateChange(
                          AthleteKeys.ExpirationDate,
                        )}
                      />
                      {errorFields.has(AthleteKeys.ExpirationDate) && (
                        <div>{errorFields.get(AthleteKeys.ExpirationDate)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.RegistrationDate]}
                    </Form.Label>
                    <Col>
                      <DatePicker
                        selected={new Date(input[AthleteKeys.RegistrationDate])}
                        onChange={this.handleDateChange(
                          AthleteKeys.RegistrationDate,
                        )}
                      />
                      {errorFields.has(AthleteKeys.RegistrationDate) && (
                        <div>
                          {errorFields.get(AthleteKeys.RegistrationDate)}
                        </div>
                      )}
                    </Col>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group as={Row}>
                    <Form.Label column>
                      <label>{AthleteTranslations[AthleteKeys.IdNumber]}</label>
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.IdNumber]}
                        onChange={this.handleChange(AthleteKeys.IdNumber)}
                      />
                      {errorFields.has(AthleteKeys.IdNumber) && (
                        <div>{errorFields.get(AthleteKeys.IdNumber)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.Association]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.Association]}
                        onChange={this.handleChange(AthleteKeys.Association)}
                      />
                      {errorFields.has(AthleteKeys.Association) && (
                        <div>{errorFields.get(AthleteKeys.Association)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.LicenseNumber]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.LicenseNumber]}
                        onChange={this.handleChange(AthleteKeys.LicenseNumber)}
                      />
                      {errorFields.has(AthleteKeys.LicenseNumber) && (
                        <div>{errorFields.get(AthleteKeys.LicenseNumber)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.IssuingAuthority]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.IssuingAuthority]}
                        onChange={this.handleChange(
                          AthleteKeys.IssuingAuthority,
                        )}
                      />
                      {errorFields.has(AthleteKeys.IssuingAuthority) && (
                        <div>
                          {errorFields.get(AthleteKeys.IssuingAuthority)}
                        </div>
                      )}
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.PostalCode]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.PostalCode]}
                        onChange={this.handleChange(AthleteKeys.PostalCode)}
                      />
                      {errorFields.has(AthleteKeys.PostalCode) && (
                        <div>{errorFields.get(AthleteKeys.PostalCode)}</div>
                      )}
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row}>
                    <Form.Label column>
                      {AthleteTranslations[AthleteKeys.Amka]}
                    </Form.Label>
                    <Col>
                      <input
                        value={input[AthleteKeys.Amka]}
                        onChange={this.handleChange(AthleteKeys.Amka)}
                      />
                      {errorFields.has(AthleteKeys.Amka) && (
                        <div>{errorFields.get(AthleteKeys.Amka)}</div>
                      )}
                    </Col>
                  </Form.Group>
                </Col>
              </Row>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={this.handleSubmit}>
              Submit
            </Button>
            <Button variant="secondary" onClick={this.props.closeModal}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
