import * as React from "react";
import { NavigationComponent } from "../components/navigation.component";
import { AthleteTableStore } from "../stores/athlete-table.store";
import { inject, observer } from "mobx-react";
import { InputAthleteModal } from "./input-athlete.modal";
import { Table, Column, Cell } from "@blueprintjs/table";
import {
  AthleteKeys,
  AthleteTranslations
} from "../../../main/ipc-server/models/athlete.interface";
import { PaginationComponent } from "../components/pagination.component";
import { Button } from "@blueprintjs/core";
import { DeleteComponent } from "../components/delete.component";
import { AthleteGunTableModal } from "./athlete-gun-table.modal";

interface AthleteTableProps {
  athleteTableStore?: AthleteTableStore;
}

@inject("athleteTableStore")
@observer
export class AthleteTablePage extends React.Component<AthleteTableProps> {
  componentWillMount = () => {
    this.props.athleteTableStore!.selectPageForAthletes(0);
  };

  rowNameRender = (key: string) => {
    return (rowIndex: number) => {
      const store = this.props.athleteTableStore;
      const list = store!.athleteList;
      if (key === "id") {
        const id = list[rowIndex][key];
        return (
          <Cell>
            <React.Fragment>
              <Button onClick={store!.showEditModal(id)}>Edit</Button>
              <Button onClick={store!.selectToDeleteAthlete(id)}>Del</Button>
              <Button onClick={store!.selectAthleteGuns(id)}>Guns</Button>
            </React.Fragment>
          </Cell>
        );
      }
      return <Cell>{list[rowIndex][key]} </Cell>;
    };
  };

  render() {
    const store = this.props.athleteTableStore;
    const listSize = store!.athleteList.length;
    const items = Object.keys(AthleteKeys).map((v, index) => {
      return (
        <Column
          key={index}
          name={AthleteTranslations[AthleteKeys[v]]}
          cellRenderer={this.rowNameRender(AthleteKeys[v])}
        />
      );
    });
    return (
      <div>
        <NavigationComponent selectedLink={"/athleteTable"} />
        <div>
          <Button onClick={store!.showAddModal}>Νέος Αθλητής</Button>
          <InputAthleteModal
            showModal={store!.showInputForm}
            closeModal={store!.closeModal}
            afterAdd={store!.afterAddingAthlete}
          />
          <DeleteComponent
            delete={store!.deleteAthlete}
            closeModal={store!.closeModal}
            showModal={store!.isDeleteFormOpened}
          />
          <AthleteGunTableModal
            closeModal={store!.closeModal}
            showModal={store!.isGunsFormOpened}
          />
        </div>
        <div>
          <Table numRows={listSize}>{items}</Table>
        </div>
        <PaginationComponent
          sizePage={store!.sizePage}
          changeSizePage={store!.changeSizePage}
          totalNumberPages={store!.totalPages}
          currentPage={store!.currentPage}
          selectPage={store!.selectPageForAthletes}
        />
      </div>
    );
  }
}
