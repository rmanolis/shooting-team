import * as React from "react";
import { Pagination, Form, Container, Row, Col } from "react-bootstrap";

interface PaginationProps {
  totalNumberPages: number;
  currentPage: number;
  sizePage: number;
  changeSizePage: (size: number) => void;
  selectPage: (i: number) => void;
}

export class PaginationComponent extends React.Component<PaginationProps> {
  onSize = (e: React.FormEvent) => {
    const elem = e.target as HTMLSelectElement;
    this.props.changeSizePage(parseInt(elem.value));
  };
  renderSelect() {
    return (
      <Form>
        <Form.Group>
          <Form.Label>Size</Form.Label>
          <Form.Control
            onChange={this.onSize}
            as="select"
            value={String(this.props.sizePage)}
          >
            <option value="10">10</option>
            <option value="100">100</option>
            <option value="1000">1000</option>
            <option value="10000">10000</option>
          </Form.Control>
        </Form.Group>
      </Form>
    );
  }
  render() {
    const { selectPage, totalNumberPages, currentPage } = this.props;
    console.log(
      "totalNumberPages",
      totalNumberPages,
      "currentPage",
      currentPage
    );
    const pageNumbers = [...Array(totalNumberPages).keys()];
    const prevNumbers = pageNumbers
      .filter(x => x < currentPage)
      .reverse()
      .slice(0, 3)
      .reverse()
      .map(num => {
        return (
          <Pagination.Item
            key={num}
            onClick={() => {
              selectPage(num);
            }}
          >
            {num}
          </Pagination.Item>
        );
      });
    const afterNumbers = pageNumbers
      .filter(x => x > currentPage)
      .slice(0, 3)
      .map(num => {
        return (
          <Pagination.Item
            key={num}
            onClick={() => {
              selectPage(num);
            }}
          >
            {num}
          </Pagination.Item>
        );
      });

    return (
      <Container>
        <Row>
          <Col>
            <Pagination>
              <Pagination.First
                disabled={currentPage == 0}
                onClick={() => {
                  selectPage(0);
                }}
              />
              <Pagination.Prev
                disabled={currentPage == 0}
                onClick={() => {
                  selectPage(currentPage - 1);
                }}
              />
              {prevNumbers}
              <Pagination.Item active>{currentPage}</Pagination.Item>
              {afterNumbers}
              <Pagination.Next
                disabled={currentPage == totalNumberPages - 1}
                onClick={() => {
                  selectPage(currentPage + 1);
                }}
              />
              <Pagination.Last
                disabled={currentPage == totalNumberPages - 1}
                onClick={() => {
                  selectPage(totalNumberPages - 1);
                }}
              />
            </Pagination>
          </Col>
          <Col>{this.renderSelect()}</Col>
        </Row>
      </Container>
    );
  }
}
