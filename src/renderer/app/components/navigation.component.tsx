import * as React from "react";
import { Link } from "react-router-dom";
import { Navbar, Alignment, Button } from "@blueprintjs/core";

interface NavigationProps {
  selectedLink: string;
}
export class NavigationComponent extends React.Component<NavigationProps> {
  render() {
    const links = [
      { link: "/", name: "Home" },
      { link: "/ammoTable", name: "Πίνακας Φυσιγγίων" },
      { link: "/athleteTable", name: "Πίνακας Αθλητών" },
      { link: "/shootingRangeTable", name: "Πίνακας Σκοπευτηρίου" }
    ];
    return (
      <Navbar>
        <Navbar.Group align={Alignment.LEFT}>
          <Navbar.Heading>Shooting Team</Navbar.Heading>
          <Navbar.Divider />
          {links.map((value, index) => {
            return (
              <Button
                className="bp3-minimal"
                key={index}
                active={this.props.selectedLink == value.link}
              >
                <Link to={value.link}>{value.name}</Link>
              </Button>
            );
          })}
        </Navbar.Group>
      </Navbar>
    );
  }
}
