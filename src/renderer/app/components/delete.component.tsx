import * as React from 'react';
import { Modal, Button } from 'react-bootstrap';

interface DeleteComponentProps {
  showModal: boolean;
  closeModal: () => void;
  delete: () => void;
}

export class DeleteComponent extends React.Component<DeleteComponentProps> {
  render() {
    return (
      <div>
        <Modal
          show={this.props.showModal}
          size="sm"
          onHide={this.props.closeModal}
        >
          <Modal.Header closeButton>
            <Modal.Title>Διαγραφή</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <span>Είστε σίγουρος για την διαγραφή;</span>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={this.props.delete}>
              Delete
            </Button>
            <Button variant="secondary" onClick={this.props.closeModal}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
