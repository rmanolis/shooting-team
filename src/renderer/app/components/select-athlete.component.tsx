import * as React from 'react';
import { Component } from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { Cell, Table, Column } from '@blueprintjs/table';
import { ipcRenderer } from 'electron';
import {
  SEARCH_ATHLETE_RESP,
  SEARCH_ATHLETE_REQ,
} from '../../../main/ipc-server/const-routes';
import { AthleteI } from '../../../main/ipc-server/models/athlete.interface';

interface SelectAthleteComponentProps {
  selectAthlete: (athlete: AthleteI) => void;
}

@observer
export class SelectAthleteComponent extends Component<
  SelectAthleteComponentProps
> {
  @observable private searchable = '';
  @observable private athletes: Array<AthleteI> = [];
  constructor(props: SelectAthleteComponentProps, content: any) {
    super(props, content);
  }

  handleSearchableChange = ({
    currentTarget: { value },
  }: React.SyntheticEvent<HTMLInputElement>) => {
    this.searchable = value;
    if (value.length < 3) {
      this.athletes = [];
    } else {
      ipcRenderer.once(SEARCH_ATHLETE_RESP, (_: Event, aths: AthleteI[]) => {
        console.log('search athletes ', aths);
        this.athletes = aths;
      });
      ipcRenderer.send(SEARCH_ATHLETE_REQ, value);
    }
  };

  nameRender = (rowIndex: number) => {
    const row = this.athletes[rowIndex];
    return <Cell>{row.lastName + ' ' + row.firstName} </Cell>;
  };

  athleteIdNumberRender = (rowIndex: number) => {
    const row = this.athletes[rowIndex];
    return <Cell>{row.idNumber} </Cell>;
  };

  selectAthlete = (athlete: AthleteI) => {
    return () => {
      this.props.selectAthlete(athlete);
    };
  };
  selectButtonRender = (rowIndex: number) => {
    const row = this.athletes[rowIndex];
    return (
      <Cell>
        <React.Fragment>
          <button onClick={this.selectAthlete(row)}>επιλογή</button>
        </React.Fragment>
      </Cell>
    );
  };

  render() {
    return (
      <div>
        <div>
          <label>Search</label>
          <input
            value={this.searchable}
            onChange={this.handleSearchableChange}
          />
        </div>
        <br />
        <div>
          <Table numRows={this.athletes.length}>
            <Column
              name="Αριθμός ταυτότητας"
              cellRenderer={this.athleteIdNumberRender}
            />
            <Column name="Όνομα" cellRenderer={this.nameRender} />
            <Column name="Επιλογή" cellRenderer={this.selectButtonRender} />
          </Table>
        </div>
      </div>
    );
  }
}
