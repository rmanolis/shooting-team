import * as React from "react";
import { Component } from "react";
import { observer } from "mobx-react";
import { Button, Modal, Container, Row, Col, ListGroup } from "react-bootstrap";
import { DateEntityI } from "../../../main/ipc-server/models/date.interface";
import { observable } from "mobx";

interface SelectDateComponentProps {
  shootingRangeDates: DateEntityI[];
  pushShootingRangeDates: () => Promise<boolean>; // action that populates the dates, if it does not have more will return false
  showModal: boolean;
  closeModal: () => void;
  afterSelection: (std: DateEntityI) => void;
}

@observer
export class SelectDateComponent extends Component<SelectDateComponentProps> {
  @observable private becameEmpty = false;
  @observable private selected?: DateEntityI = undefined;

  selectDate = (std: DateEntityI) => {
    return () => {
      this.selected = std;
    };
  };

  moreDates = () => {
    this.props.pushShootingRangeDates().then(pushedDates => {
      this.becameEmpty = !pushedDates;
    });
  };

  closeModal = () => {
    this.props.closeModal();
    this.selected = undefined;
    this.becameEmpty = false;
  };

  handleSubmit = () => {
    if (this.selected) {
      this.props.afterSelection(this.selected);
      this.selected = undefined;
      this.becameEmpty = false;
    } else {
      alert("Επέλεξε ημερομηνία");
    }
  };

  showMoreButton = () => {
    if (this.becameEmpty) {
      return <span>More</span>;
    } else {
      return <Button onClick={this.moreDates}>More</Button>;
    }
  };

  render() {
    const sdt = this.selected;
    let selectedDate = "";
    if (sdt) {
      selectedDate = new Date(sdt.forDate).toLocaleDateString("en-US");
    }
    const dts = this.props.shootingRangeDates;
    const htmlDts = dts.map((v, i) => {
      const strDate = new Date(v.forDate).toLocaleDateString("en-US");
      return (
        <ListGroup.Item
          key={i}
          onClick={this.selectDate(v)}
          active={strDate == selectedDate}
        >
          {strDate}
        </ListGroup.Item>
      );
    });
    htmlDts.push(
      <ListGroup.Item key={htmlDts.length}>
        {this.showMoreButton()}
      </ListGroup.Item>
    );
    return (
      <Modal show={this.props.showModal} onHide={this.closeModal}>
        <Modal.Body>
          <Container>
            <Row>
              <Col sm="4">Επιλεγμένη:</Col>
              <Col sm="4">{selectedDate}</Col>
            </Row>
            <Row>
              <Col>
                <ListGroup
                  style={{
                    maxHeight: "400px",
                    overflowY: "auto"
                  }}
                >
                  {htmlDts}
                </ListGroup>
              </Col>
            </Row>
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={this.handleSubmit}>
            Submit
          </Button>
          <Button variant="secondary" onClick={this.closeModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
