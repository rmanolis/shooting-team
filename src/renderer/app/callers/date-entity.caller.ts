import { ErrorI } from "../../../main/ipc-server/models/common.interface";
import { DateEntityI } from "../../../main/ipc-server/models/date.interface";
import { ipcRenderer } from "electron";
import {
  GET_DATE_ENTITY_FOR_TODAY_RESP,
  GET_DATE_ENTITY_FOR_TODAY_REQ,
  GET_DATE_ENTITIES_PAGINATED_RESP,
  GET_DATE_ENTITIES_PAGINATED_REQ
} from "../../../main/ipc-server/const-routes";

export function getTodayDate(): Promise<ErrorI | DateEntityI> {
  return new Promise(res => {
    ipcRenderer.once(
      GET_DATE_ENTITY_FOR_TODAY_RESP,
      (_: Event, today: ErrorI | DateEntityI) => {
        res(today);
      }
    );
    ipcRenderer.send(GET_DATE_ENTITY_FOR_TODAY_REQ);
  });
}

export function getDateEntitiesPaginated(
  limit: number,
  lastId?: number
): Promise<DateEntityI[]> {
  return new Promise(res => {
    ipcRenderer.once(
      GET_DATE_ENTITIES_PAGINATED_RESP,
      (_: Event, lmp: DateEntityI[]) => {
        res(lmp);
      }
    );
    ipcRenderer.send(GET_DATE_ENTITIES_PAGINATED_REQ, limit, lastId);
  });
}
