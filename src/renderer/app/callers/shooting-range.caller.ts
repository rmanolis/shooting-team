import {
  ErrorI,
  PaginationI,
  ListPaginatedI
} from "../../../main/ipc-server/models/common.interface";
import {
  InputShootingRangeI,
  ShootingRangeI
} from "../../../main/ipc-server/models/shooting-range.interface";
import { ipcRenderer } from "electron";
import {
  ADD_SHOOTING_RANGE_RESP,
  ADD_SHOOTING_RANGE_REQ,
  EDIT_SHOOTING_RANGE_REQ,
  EDIT_SHOOTING_RANGE_RESP,
  GET_SHOOTING_RANGES_BY_DATE_PAGINATED_RESP,
  GET_SHOOTING_RANGES_BY_DATE_PAGINATED_REQ,
  GET_SHOOTING_RANGE_RESP,
  GET_SHOOTING_RANGE_REQ,
  DELETE_SHOOTING_RANGE_RESP,
  DELETE_SHOOTING_RANGE_REQ
} from "../../../main/ipc-server/const-routes";

export function addInputShootingRange(
  input: InputShootingRangeI
): Promise<ErrorI | ShootingRangeI> {
  return new Promise(res => {
    ipcRenderer.once(
      ADD_SHOOTING_RANGE_RESP,
      (_: Event, output: ErrorI | ShootingRangeI) => {
        res(output);
      }
    );
    ipcRenderer.send(ADD_SHOOTING_RANGE_REQ, input);
  });
}

export function editInputShootingRange(
  id: number,
  input: InputShootingRangeI
): Promise<ErrorI | ShootingRangeI> {
  return new Promise(res => {
    ipcRenderer.once(
      EDIT_SHOOTING_RANGE_RESP,
      (_: Event, output: ErrorI | ShootingRangeI) => {
        res(output);
      }
    );
    ipcRenderer.send(EDIT_SHOOTING_RANGE_REQ, id, input);
  });
}

export function getShootingRangesPaginatedByDate(
  dateID: number,
  pag: PaginationI
): Promise<ListPaginatedI<ShootingRangeI>> {
  return new Promise(res => {
    ipcRenderer.once(
      GET_SHOOTING_RANGES_BY_DATE_PAGINATED_RESP,
      (_: Event, lmp: ListPaginatedI<ShootingRangeI>) => {
        res(lmp);
      }
    );
    ipcRenderer.send(GET_SHOOTING_RANGES_BY_DATE_PAGINATED_REQ, dateID, pag);
  });
}

export function getShootingRange(id: number): Promise<ShootingRangeI> {
  return new Promise(res => {
    ipcRenderer.once(
      GET_SHOOTING_RANGE_RESP,
      (_: Event, sr: ShootingRangeI) => {
        res(sr);
      }
    );
    ipcRenderer.send(GET_SHOOTING_RANGE_REQ, id);
  });
}

export function deleteShootingRange(id: number): Promise<ErrorI | null> {
  return new Promise(res => {
    ipcRenderer.once(
      DELETE_SHOOTING_RANGE_RESP,
      (_: Event, out: ErrorI | null) => {
        res(out);
      }
    );
    ipcRenderer.send(DELETE_SHOOTING_RANGE_REQ, id);
  });
}
