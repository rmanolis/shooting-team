import {
  ErrorI,
  PaginationI,
  ListPaginatedI
} from "../../../main/ipc-server/models/common.interface";
import { ipcRenderer } from "electron";
import {
  ADD_AMMO_RESP,
  ADD_AMMO_REQ,
  EDIT_AMMO_RESP,
  EDIT_AMMO_REQ,
  GET_AMMOS_BY_DATE_PAGINATED_RESP,
  GET_AMMOS_BY_DATE_PAGINATED_REQ,
  GET_AMMO_RESP,
  GET_AMMO_REQ,
  DELETE_AMMO_RESP,
  DELETE_AMMO_REQ
} from "../../../main/ipc-server/const-routes";
import {
  InputAmmoI,
  AmmoI
} from "../../../main/ipc-server/models/ammo.interface";

export function addInputAmmo(input: InputAmmoI): Promise<ErrorI | AmmoI> {
  return new Promise(res => {
    ipcRenderer.once(ADD_AMMO_RESP, (_: Event, output: ErrorI | AmmoI) => {
      res(output);
    });
    ipcRenderer.send(ADD_AMMO_REQ, input);
  });
}

export function editInputAmmo(
  id: number,
  input: InputAmmoI
): Promise<ErrorI | AmmoI> {
  return new Promise(res => {
    ipcRenderer.once(EDIT_AMMO_RESP, (_: Event, output: ErrorI | AmmoI) => {
      res(output);
    });
    ipcRenderer.send(EDIT_AMMO_REQ, id, input);
  });
}

export function getAmmosPaginatedByDate(
  dateID: number,
  pag: PaginationI
): Promise<ListPaginatedI<AmmoI>> {
  return new Promise(res => {
    ipcRenderer.once(
      GET_AMMOS_BY_DATE_PAGINATED_RESP,
      (_: Event, lmp: ListPaginatedI<AmmoI>) => {
        res(lmp);
      }
    );
    ipcRenderer.send(GET_AMMOS_BY_DATE_PAGINATED_REQ, dateID, pag);
  });
}

export function getAmmo(id: number): Promise<AmmoI> {
  return new Promise(res => {
    ipcRenderer.once(GET_AMMO_RESP, (_: Event, sr: AmmoI) => {
      res(sr);
    });
    ipcRenderer.send(GET_AMMO_REQ, id);
  });
}

export function deleteAmmo(id: number): Promise<ErrorI | null> {
  return new Promise(res => {
    ipcRenderer.once(DELETE_AMMO_RESP, (_: Event, out: ErrorI | null) => {
      res(out);
    });
    ipcRenderer.send(DELETE_AMMO_REQ, id);
  });
}
