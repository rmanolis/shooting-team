import {
  InputAthleteGunI,
  AthleteGunI,
} from '../../../main/ipc-server/models/athlete-gun.interface';
import { ErrorI } from '../../../main/ipc-server/models/common.interface';
import {
  ADD_ATHLETE_GUN_RESP,
  ADD_ATHLETE_GUN_REQ,
  DELETE_ATHLETE_GUN_REQ,
  DELETE_ATHLETE_GUN_RESP,
  GET_TOTAL_ATHLETE_GUNS_RESP,
  GET_TOTAL_ATHLETE_GUNS_REQ,
} from '../../../main/ipc-server/const-routes';
import { ipcRenderer } from 'electron';

export function addAthleteGun(
  input: InputAthleteGunI,
): Promise<ErrorI | AthleteGunI> {
  return new Promise(res => {
    ipcRenderer.once(
      ADD_ATHLETE_GUN_RESP,
      (_: Event, out: ErrorI | AthleteGunI) => {
        res(out);
      },
    );
    ipcRenderer.send(ADD_ATHLETE_GUN_REQ, input);
  });
}

export function deleteAthleteGun(id: number): Promise<ErrorI | null> {
  return new Promise(res => {
    ipcRenderer.once(
      DELETE_ATHLETE_GUN_RESP,
      (_: Event, out: ErrorI | null) => {
        res(out);
      },
    );
    ipcRenderer.send(DELETE_ATHLETE_GUN_REQ, id);
  });
}

export function getGunsByAthlete(athleteId: number): Promise<AthleteGunI[]> {
  return new Promise(res => {
    ipcRenderer.once(
      GET_TOTAL_ATHLETE_GUNS_RESP,
      (_: Event, ags: AthleteGunI[]) => {
        res(ags);
      },
    );
    ipcRenderer.send(GET_TOTAL_ATHLETE_GUNS_REQ, athleteId);
  });
}
