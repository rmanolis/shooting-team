import {
  InputAthleteI,
  AthleteI
} from "../../../main/ipc-server/models/athlete.interface";
import {
  ErrorI,
  PaginationI,
  ListPaginatedI
} from "../../../main/ipc-server/models/common.interface";
import { ipcRenderer } from "electron";
import {
  ADD_ATHLETE_RESP,
  ADD_ATHLETE_REQ,
  EDIT_ATHLETE_RESP,
  EDIT_ATHLETE_REQ,
  DELETE_ATHLETE_RESP,
  DELETE_ATHLETE_REQ,
  GET_ATHLETE_RESP,
  GET_ATHLETE_REQ,
  GET_ATHLETES_PAGINATED_RESP,
  GET_ATHLETES_PAGINATED_REQ,
  SEARCH_ATHLETE_RESP,
  SEARCH_ATHLETE_REQ
} from "../../../main/ipc-server/const-routes";

export function addAthlete(athlete: InputAthleteI): Promise<ErrorI | AthleteI> {
  return new Promise(res => {
    ipcRenderer.once(ADD_ATHLETE_RESP, (_: Event, out: ErrorI | AthleteI) => {
      res(out);
    });
    ipcRenderer.send(ADD_ATHLETE_REQ, athlete);
  });
}

export function editAthlete(
  id: number,
  athlete: InputAthleteI
): Promise<ErrorI | AthleteI> {
  return new Promise(res => {
    ipcRenderer.once(EDIT_ATHLETE_RESP, (_: Event, out: ErrorI | AthleteI) => {
      res(out);
    });
    ipcRenderer.send(EDIT_ATHLETE_REQ, id, athlete);
  });
}

export function deleteAthlete(id: number): Promise<ErrorI | null> {
  return new Promise(res => {
    ipcRenderer.once(DELETE_ATHLETE_RESP, (_: Event, out: ErrorI | null) => {
      res(out);
    });
    ipcRenderer.send(DELETE_ATHLETE_REQ, id);
  });
}

export function getAthlete(id: number): Promise<ErrorI | AthleteI> {
  return new Promise(res => {
    ipcRenderer.once(GET_ATHLETE_RESP, (_: Event, out: ErrorI | AthleteI) => {
      res(out);
    });
    ipcRenderer.send(GET_ATHLETE_REQ, id);
  });
}

export function getAthletesPaginated(
  pag: PaginationI
): Promise<ListPaginatedI<AthleteI>> {
  return new Promise(res => {
    ipcRenderer.once(
      GET_ATHLETES_PAGINATED_RESP,
      (_: Event, lmp: ListPaginatedI<AthleteI>) => {
        res(lmp);
      }
    );
    ipcRenderer.send(GET_ATHLETES_PAGINATED_REQ, pag);
  });
}

export function searchAthlete(value: string): Promise<AthleteI[]> {
  return new Promise(res => {
    ipcRenderer.once(SEARCH_ATHLETE_RESP, (_: Event, aths: AthleteI[]) => {
      res(aths);
    });
    ipcRenderer.send(SEARCH_ATHLETE_REQ, value);
  });
}
