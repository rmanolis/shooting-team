import * as React from "react";
import * as ReactDOM from "react-dom";
import { AppContainer } from "react-hot-loader";

require("!style-loader!css-loader!bootstrap/dist/css/bootstrap.min.css");
require("!style-loader!css-loader!@blueprintjs/icons/lib/css/blueprint-icons.css");
require("!style-loader!css-loader!@blueprintjs/core/lib/css/blueprint.css");
require("!style-loader!css-loader!@blueprintjs/table/lib/css/table.css");
require("!style-loader!css-loader!@blueprintjs/table/lib/css/table.css");
require("!style-loader!css-loader!react-datepicker/dist/react-datepicker.min.css");

/*
import './styles/app.css'
import './styles/blueprint-icons.css'
import './styles/blueprint.css'
import './styles/bootstrap.min.css'
import './styles/normalize.css'
import './styles/react-datepicker.min.css'*/

let render = () => {
  const { App } = require("./app");
  ReactDOM.render(
    <AppContainer>
      <App />
    </AppContainer>,
    document.getElementById("app")
  );
};

render();
if (module.hot) {
  module.hot.accept(render);
}
