import { app, BrowserWindow } from "electron";
import { createConnection } from "typeorm";
import * as path from "path";
import * as os from "os";
import { format as formatUrl } from "url";
import { router } from "./ipc-server/IpcRoutes";
import { Athlete } from "./ipc-server/entities/athlete.entity";
import { Ammo } from "./ipc-server/entities/ammo.entity";
import { DateEntity } from "./ipc-server/entities/date.entity";
import { AthleteGun } from "./ipc-server/entities/athlete-gun.entity";
import { ShootingRange } from "./ipc-server/entities/shooting-range.entity";

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow: Electron.BrowserWindow | null = null;

const isDevelopment = process.env.NODE_ENV !== "production";
const createWindow = async () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 2000,
    height: 900
  });

  // and load the index.html of the app.
  if (isDevelopment) {
    mainWindow.loadURL(
      `http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`
    );
  } else {
    mainWindow.loadURL(
      formatUrl({
        pathname: path.join(__dirname, "index.html"),
        protocol: "file",
        slashes: true
      })
    );
  }
  // Open the DevTools.
  if (isDevelopment) {
    mainWindow.webContents.openDevTools();
  }

  console.log(
    "Opening DB ",
    path.normalize(os.homedir() + "/shooting-team.db")
  );
  const db = await createConnection({
    type: "sqlite",
    synchronize: true,
    logging: true,
    logger: "simple-console",
    database: path.normalize(os.homedir() + "/shooting-team.db"),
    entities: [Athlete, Ammo, DateEntity, AthleteGun, ShootingRange]
  });
  // start the IPC router
  router(db).then();
  // Emitted when the window is closed.
  mainWindow.on("closed", async () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
    await db.close();
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
