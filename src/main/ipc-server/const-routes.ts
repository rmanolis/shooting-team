// for members
export const ADD_MEMBER_REQ = "add-member",
  ADD_MEMBER_RESP = "add-member-resp";

export const GET_TOTAL_MEMBERS_REQ = "get-total-members",
  GET_TOTAL_MEMBERS_RESP = "get-total-members-resp";

export const GET_MEMBERS_PAGINATED_REQ = "get-members-paginated",
  GET_MEMBERS_PAGINATED_RESP = "get-members-paginated-resp";

export const SEARCH_MEMBER_REQ = "search-member",
  SEARCH_MEMBER_RESP = "search-member-resp";

// for athletes
export const ADD_ATHLETE_REQ = "add-athlete",
  ADD_ATHLETE_RESP = "add-athlete-resp";

export const GET_ATHLETES_PAGINATED_REQ = "get-athletes-paginated",
  GET_ATHLETES_PAGINATED_RESP = "get-athletes-paginated-resp";

export const EDIT_ATHLETE_REQ = "edit-athlete",
  EDIT_ATHLETE_RESP = "edit-athlete-resp";

export const DELETE_ATHLETE_REQ = "delete-athlete",
  DELETE_ATHLETE_RESP = "delete-athlete-resp";

export const GET_ATHLETE_REQ = "get-athlete",
  GET_ATHLETE_RESP = "get-athlete-resp";

export const SEARCH_ATHLETE_REQ = "search-athlete",
  SEARCH_ATHLETE_RESP = "search-athlete-resp";

// for athlete's guns
export const ADD_ATHLETE_GUN_REQ = "add-athlete-gun",
  ADD_ATHLETE_GUN_RESP = "add-athlete-gun-resp";

export const DELETE_ATHLETE_GUN_REQ = "delete-athlete-gun",
  DELETE_ATHLETE_GUN_RESP = "delete-athlete-gun-resp";

export const GET_TOTAL_ATHLETE_GUNS_REQ = "get-total-athlete-guns",
  GET_TOTAL_ATHLETE_GUNS_RESP = "get-total-athlete-guns-resp";

// for shooting range's guns
export const ADD_SHOOTING_RANGE_REQ = "add-shooting-range",
  ADD_SHOOTING_RANGE_RESP = "add-shooting-range-resp";

export const GET_SHOOTING_RANGES_PAGINATED_REQ =
    "get-shooting-ranges-paginated",
  GET_SHOOTING_RANGES_PAGINATED_RESP = "get-shooting-ranges-paginated-resp";

export const EDIT_SHOOTING_RANGE_REQ = "edit-shooting-range",
  EDIT_SHOOTING_RANGE_RESP = "edit-shooting-range-resp";

export const DELETE_SHOOTING_RANGE_REQ = "delete-shooting-range",
  DELETE_SHOOTING_RANGE_RESP = "delete-shooting-range-resp";

export const GET_SHOOTING_RANGE_REQ = "get-shooting-range",
  GET_SHOOTING_RANGE_RESP = "get-shooting-range-resp";

export const GET_SHOOTING_RANGES_BY_DATE_PAGINATED_REQ =
    "get-shooting-ranges-by-date-paginated",
  GET_SHOOTING_RANGES_BY_DATE_PAGINATED_RESP =
    "get-shooting-ranges-by-date-paginated-resp";

// for ammo
export const ADD_AMMO_REQ = "add-ammo",
  ADD_AMMO_RESP = "add-ammo-resp";

export const GET_AMMO_PAGINATED_REQ = "get-ammos-paginated",
  GET_AMMO_PAGINATED_RESP = "get-ammos-paginated-resp";

export const GET_AMMOS_BY_DATE_PAGINATED_REQ = "get-ammos-by-date-paginated",
  GET_AMMOS_BY_DATE_PAGINATED_RESP = "get-ammos-by-date-paginated-resp";

export const EDIT_AMMO_REQ = "edit-ammo",
  EDIT_AMMO_RESP = "edit-ammo-resp";

export const DELETE_AMMO_REQ = "delete-ammo",
  DELETE_AMMO_RESP = "delete-ammo-resp";

export const GET_AMMO_REQ = "get-ammo",
  GET_AMMO_RESP = "get-ammo-resp";

// for date entity
export const GET_DATE_ENTITY_FOR_TODAY_REQ = "get-date-entity-for-today",
  GET_DATE_ENTITY_FOR_TODAY_RESP = "get-date-entity-for-today-resp";

export const GET_DATE_ENTITIES_PAGINATED_REQ = "get-date-entities-paginated",
  GET_DATE_ENTITIES_PAGINATED_RESP = "get-date-entities-paginated-resp";
