import "reflect-metadata";
import { ipcMain } from "electron";

import { Connection } from "typeorm";
import {
  ADD_ATHLETE_REQ,
  GET_ATHLETES_PAGINATED_REQ,
  DELETE_ATHLETE_REQ,
  EDIT_ATHLETE_REQ,
  GET_ATHLETE_REQ,
  SEARCH_ATHLETE_REQ,
  ADD_ATHLETE_GUN_REQ,
  DELETE_ATHLETE_GUN_REQ,
  GET_TOTAL_ATHLETE_GUNS_REQ,
  ADD_SHOOTING_RANGE_REQ,
  EDIT_SHOOTING_RANGE_REQ,
  DELETE_SHOOTING_RANGE_REQ,
  GET_SHOOTING_RANGES_PAGINATED_REQ,
  GET_SHOOTING_RANGE_REQ,
  ADD_AMMO_REQ,
  DELETE_AMMO_REQ,
  GET_AMMO_PAGINATED_REQ,
  GET_AMMOS_BY_DATE_PAGINATED_REQ,
  GET_AMMO_REQ,
  EDIT_AMMO_REQ,
  GET_SHOOTING_RANGES_BY_DATE_PAGINATED_REQ,
  GET_DATE_ENTITY_FOR_TODAY_REQ,
  GET_DATE_ENTITIES_PAGINATED_REQ
} from "./const-routes";
import { AthleteController } from "./ctrls/athlete.controller";
import { AthleteGunController } from "./ctrls/athlete-gun.controller";
import { ShootingRangeController } from "./ctrls/shooting-range.controller";
import { AmmoController } from "./ctrls/ammo.controller";
import { DateEntityController } from "./ctrls/date.controller";

export async function router(db: Connection) {
  // for athletes
  const athleteCtrls = new AthleteController(db);
  ipcMain.on(ADD_ATHLETE_REQ, await athleteCtrls.add());
  ipcMain.on(GET_ATHLETES_PAGINATED_REQ, await athleteCtrls.getPaginated());
  ipcMain.on(DELETE_ATHLETE_REQ, await athleteCtrls.deleteAthlete());
  ipcMain.on(EDIT_ATHLETE_REQ, await athleteCtrls.editAthlete());
  ipcMain.on(GET_ATHLETE_REQ, await athleteCtrls.getAthlete());
  ipcMain.on(SEARCH_ATHLETE_REQ, await athleteCtrls.searchAthlete());

  // for athlete guns
  const athleteGunCtrls = new AthleteGunController(db);
  ipcMain.on(ADD_ATHLETE_GUN_REQ, await athleteGunCtrls.addAthleteGun());
  ipcMain.on(DELETE_ATHLETE_GUN_REQ, await athleteGunCtrls.deleteAthleteGun());
  ipcMain.on(
    GET_TOTAL_ATHLETE_GUNS_REQ,
    await athleteGunCtrls.getTotalAthleteGuns()
  );

  // for shooting range
  const shootingRangeCtrls = new ShootingRangeController(db);
  ipcMain.on(ADD_SHOOTING_RANGE_REQ, await shootingRangeCtrls.add());
  ipcMain.on(EDIT_SHOOTING_RANGE_REQ, await shootingRangeCtrls.edit());
  ipcMain.on(DELETE_SHOOTING_RANGE_REQ, await shootingRangeCtrls.delete());
  ipcMain.on(
    GET_SHOOTING_RANGES_PAGINATED_REQ,
    await shootingRangeCtrls.getPaginated()
  );
  ipcMain.on(GET_SHOOTING_RANGE_REQ, await shootingRangeCtrls.get());
  ipcMain.on(
    GET_SHOOTING_RANGES_BY_DATE_PAGINATED_REQ,
    await shootingRangeCtrls.getPaginatedByDateId()
  );

  // for date entity
  const dateCtrls = new DateEntityController(db);
  ipcMain.on(GET_DATE_ENTITY_FOR_TODAY_REQ, await dateCtrls.getToday());

  ipcMain.on(GET_DATE_ENTITIES_PAGINATED_REQ, await dateCtrls.getPaginated());
  // for ammo
  const ammoCtrls = new AmmoController(db);
  ipcMain.on(ADD_AMMO_REQ, await ammoCtrls.add());
  ipcMain.on(EDIT_AMMO_REQ, await ammoCtrls.edit());
  ipcMain.on(DELETE_AMMO_REQ, await ammoCtrls.delete());
  ipcMain.on(GET_AMMO_PAGINATED_REQ, await ammoCtrls.getPaginated());
  ipcMain.on(
    GET_AMMOS_BY_DATE_PAGINATED_REQ,
    await ammoCtrls.getPaginatedByDateId()
  );
  ipcMain.on(GET_AMMO_REQ, await ammoCtrls.get());
}
