import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity("athletes")
export class Athlete {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  fatherName: string;

  @Column()
  motherName: string;

  @Column()
  telephone: string;

  @Column()
  address: string;

  @Column()
  email: string;

  @Column()
  town: string;

  @Column()
  postalCode: string;

  @Column({ type: 'datetime' })
  birthDate: Date;

  @Column()
  amka: string;

  @Column()
  idNumber: string;

  @Column()
  association: string;

  @Column({ type: 'datetime' })
  registrationDate: Date;

  @Column()
  licenseNumber: string;

  @Column({ type: 'datetime' })
  issueDate: Date;

  @Column({ type: 'datetime' })
  expirationDate: Date;

  @Column()
  issuingAuthority: string;
}
