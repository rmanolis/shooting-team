import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("date_entities")
export class DateEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "date" })
  forDate: Date;
}
