import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity("athlete_guns")
export class AthleteGun {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  athleteID: number;

  @Column()
  order: number;

  @Column()
  armsNumber: string;

  @Column()
  caliber: string;
}
