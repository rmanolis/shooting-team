import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("shooting_ranges")
export class ShootingRange {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  athleteID: number;

  @Column({ type: "datetime" })
  createdDate: Date;

  @Column()
  dateEntityID: number;

  @Column()
  gunNumber: string;

  @Column()
  issuingAuthority: string;

  @Column()
  caliber: string;

  @Column()
  quantity: number;

  @Column()
  warehouseQuantity: number;

  @Column()
  isContest: boolean;

  @Column()
  isTraining: boolean;
}
