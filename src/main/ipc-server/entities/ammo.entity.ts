import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity("ammo")
export class Ammo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  athleteID: number;

  @Column({ type: "datetime" })
  createdDate: Date;

  @Column()
  dateEntityID: number;

  @Column()
  location: string;

  @Column()
  caliber: string;

  @Column()
  quantity: number;
}
