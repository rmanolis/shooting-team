import { DateEntity } from "../entities/date.entity";
import { DateEntityI } from "./date.interface";
import { Connection, LessThanOrEqual } from "typeorm";
import { ErrorI } from "./common.interface";

function _serializeDateEntity(atd: DateEntity): DateEntityI {
  const ati: DateEntityI = {
    id: atd.id,
    forDate: atd.forDate.toString()
  };
  return ati;
}

export async function getDateEntityForToday(
  db: Connection
): Promise<ErrorI | DateEntityI> {
  const srdr = await db.getRepository(DateEntity);
  const today = new Date();
  const srd = await srdr.findOne({
    where: {
      forDate: today.toISOString().split("T")[0]
    }
  });
  if (srd !== undefined) {
    return _serializeDateEntity(srd);
  } else {
    try {
      const newSrd = new DateEntity();
      newSrd.forDate = today;
      const nnsrd = await srdr.save(newSrd);
      return _serializeDateEntity(nnsrd);
    } catch (e) {
      const err = <Error>e;
      return { message: err.message, fields: [] };
    }
  }
}

export async function getDateEntitiesLimited(
  db: Connection,
  limitNum: number,
  lastId: number | null
): Promise<DateEntityI[]> {
  const ar = await db.getRepository(DateEntity);
  let query: any = {
    take: limitNum,
    order: {
      id: "DESC"
    }
  };

  if (lastId !== null) {
    query["where"] = {
      id: LessThanOrEqual(lastId)
    };
  }

  const srs = await ar.find(query);
  let newSrs: DateEntityI[] = [];
  for (let v of srs) {
    if (lastId !== null) {
      if (v.id != lastId) {
        newSrs.push(_serializeDateEntity(v));
      }
    } else {
      newSrs.push(_serializeDateEntity(v));
    }
  }

  return newSrs;
}
