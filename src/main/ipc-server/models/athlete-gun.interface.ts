export interface AthleteGunI {
  id: number;
  athleteID: number;
  order: number;
  armsNumber: string;
  caliber: string;
}

export interface InputAthleteGunI {
  athleteID: number;
  armsNumber: string;
  caliber: string;
}

export enum AthleteGunKeys {
  Order = 'order',
  ArmsNumber = 'armsNumber',
  Caliber = 'caliber',
}

export const AthleteGunTranslations = {
  order: 'Σειρά',
  armsNumber: 'Αριθμός Όπλου',
  caliber: 'Διαμέτρημα',
};
