import { _validateAthleteInput } from './athlete.model';
import { InputAthleteI } from './athlete.interface';

describe('validate athlete input', function() {
  it('validate letgth', function() {
    const ath: InputAthleteI = {
      firstName: '',
      lastName: '',
      fatherName: '',
      motherName: '',
      telephone: '',
      address: '',
      email: '',
      town: '',
      postalCode: '',
      amka: '',
      idNumber: '',
      association: '',
      licenseNumber: '',
      issuingAuthority: '',
      birthDate: new Date('1988-12-17T03:24:00'),
      issueDate: new Date('1995-12-17T03:24:00'),
      expirationDate: new Date('2029-12-17T03:24:00'),
      registrationDate: new Date('2010-12-17T03:24:00'),
    };
    let result = _validateAthleteInput(ath);
    expect(result.length).toBe(14);
  });
});
