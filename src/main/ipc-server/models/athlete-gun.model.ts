import { InputAthleteGunI, AthleteGunI } from './athlete-gun.interface';
import { ErrorFieldI, ErrorI } from './common.interface';
import { ErrValueIsEmpty, ErrValueIsZero, ErrFields } from './common.errors';
import { Connection, getConnection } from 'typeorm';
import { AthleteGun } from '../entities/athlete-gun.entity';

export function _validateAthleteGunInput(
  input: InputAthleteGunI,
): ErrorFieldI[] {
  let errorFields: ErrorFieldI[] = [];
  // validate the length of the strings
  for (let key in input) {
    const value = input[key];

    if (typeof value === 'string') {
      if (value.length === 0) {
        errorFields.push({
          field: key,
          message: ErrValueIsEmpty.message,
        });
      }
    }
    if (typeof value === 'number') {
      if (value === 0) {
        errorFields.push({
          field: key,
          message: ErrValueIsZero.message,
        });
      }
    }
  }
  if (errorFields.length > 0) {
    return errorFields;
  }

  return errorFields;
}

export async function addAthleteGun(
  db: Connection,
  input: InputAthleteGunI,
): Promise<ErrorI | InputAthleteGunI> {
  const errs = _validateAthleteGunInput(input);
  if (errs.length > 0) {
    return {
      fields: errs,
      message: ErrFields.message,
    };
  }
  const ar = await db.getRepository(AthleteGun);
  const ag = new AthleteGun();
  ag.armsNumber = input.armsNumber;
  ag.athleteID = input.athleteID;
  ag.caliber = input.caliber;
  const guns = await getTotalAthleteGuns(db, input.athleteID);
  ag.order = guns.length + 1;
  try {
    const newAth = await ar.save(ag);
    return newAth;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

async function _reorderGuns(db: Connection, athleteId: number) {
  const ar = await db.getRepository(AthleteGun);
  const ags = await ar.find({
    where: {
      athleteID: athleteId,
    },
    order: {
      order: 'ASC',
    },
  });
  for (let [i, v] of ags.entries()) {
    v.order = i + 1;
    await ar.save(v);
  }
}

export async function deleteAthleteGun(
  db: Connection,
  id: number,
): Promise<ErrorI | number> {
  try {
    const ar = await db.getRepository(AthleteGun);
    const ath = await ar.findOne({ id });
    if (ath === undefined) {
      return {
        message: 'Το όπλο του αθλητή δεν υπάρχει με το ID ' + id,
        fields: [],
      };
    }
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(AthleteGun)
      .where('id = :id', { id })
      .execute();
    await _reorderGuns(db, ath.athleteID);
    return id;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function getTotalAthleteGuns(
  db: Connection,
  athleteId: number,
): Promise<AthleteGunI[]> {
  const ar = await db.getRepository(AthleteGun);
  const ags = await ar.find({
    where: {
      athleteID: athleteId,
    },
  });
  return ags;
}
