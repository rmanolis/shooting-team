export const ErrValueIsEmpty = new Error('Η τιμή είναι κενή.'),
  ErrValueIsZero = new Error('Η τιμή είναι μηδέν.'),
  ErrEmail = new Error('Το email είναι λάθος.'),
  ErrFields = new Error('Πρόβλημα με τις τιμές.');
