import { InputAthleteI, AthleteI } from "./athlete.interface";
import {
  ErrorFieldI,
  ErrorI,
  PaginationI,
  ListPaginatedI
} from "./common.interface";
import { ErrValueIsEmpty, ErrFields } from "./common.errors";
import { Connection, getConnection } from "typeorm";
import { Athlete } from "../entities/athlete.entity";
import { getTotalNumberPages } from "./common.model";

export function _validateAthleteInput(athlete: InputAthleteI): ErrorFieldI[] {
  let errorFields: ErrorFieldI[] = [];

  // validate the length of the strings
  for (let key in athlete) {
    if (typeof athlete[key] === "string") {
      if (athlete[key].length === 0) {
        errorFields.push({
          field: key,
          message: ErrValueIsEmpty.message
        });
      }
    }
  }
  if (errorFields.length > 0) {
    return errorFields;
  }

  return errorFields;
}

export async function addAthlete(
  db: Connection,
  input: InputAthleteI
): Promise<ErrorI | AthleteI> {
  const errs = _validateAthleteInput(input);
  if (errs.length > 0) {
    return {
      fields: errs,
      message: ErrFields.message
    };
  }
  const ar = await db.getRepository(Athlete);
  const ath = new Athlete();
  ath.address = input.address;
  ath.amka = input.amka;
  ath.association = input.association;
  ath.birthDate = input.birthDate;
  ath.email = input.email;
  ath.expirationDate = input.expirationDate;
  ath.fatherName = input.fatherName;
  ath.firstName = input.firstName;
  ath.idNumber = input.idNumber;
  ath.issueDate = input.issueDate;
  ath.issuingAuthority = input.issuingAuthority;
  ath.lastName = input.lastName;
  ath.licenseNumber = input.licenseNumber;
  ath.motherName = input.motherName;
  ath.postalCode = input.postalCode;
  ath.registrationDate = input.registrationDate;
  ath.telephone = input.telephone;
  ath.town = input.town;
  try {
    const newAth = await ar.save(ath);
    return newAth;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function getAthletesPaginated(
  db: Connection,
  pag: PaginationI
): Promise<ListPaginatedI<AthleteI>> {
  const ar = await db.getRepository(Athlete);
  const totalPageNumber = await getTotalNumberPages(db, Athlete, pag);
  const aths = await ar.find({
    skip: pag.page * pag.size,
    take: pag.size,
    order: {
      id: "DESC"
    }
  });
  const res: ListPaginatedI<Athlete> = {
    page: pag.page,
    size: pag.size,
    totalPages: totalPageNumber,
    data: aths
  };
  return res;
}

export async function editAthlete(
  db: Connection,
  id: number,
  input: InputAthleteI
): Promise<ErrorI | AthleteI> {
  const errs = _validateAthleteInput(input);
  if (errs.length > 0) {
    return {
      fields: errs,
      message: ErrFields.message
    };
  }
  try {
    const ar = await db.getRepository(Athlete);
    const ath = await ar.findOne({ id });
    if (ath === undefined) {
      return { message: "Ο αθλητής δεν υπάρχει με το ID " + id, fields: [] };
    }
    const newAth = { ...ath, ...input };
    const fromDbAth = await ar.save(newAth);
    return fromDbAth;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function deleteAthlete(
  db: Connection,
  id: number
): Promise<ErrorI | number> {
  try {
    const ar = await db.getRepository(Athlete);
    const ath = await ar.findOne({ id });
    if (ath === undefined) {
      return { message: "Ο αθλητής δεν υπάρχει με το ID " + id, fields: [] };
    }
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Athlete)
      .where("id = :id", { id })
      .execute();
    return id;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function getAthlete(
  db: Connection,
  id: number
): Promise<ErrorI | AthleteI> {
  try {
    const ar = await db.getRepository(Athlete);
    const ath = await ar.findOne({ id });
    if (ath === undefined) {
      return { message: "Ο αθλητής δεν υπάρχει με το ID " + id, fields: [] };
    }
    return ath;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function searchAthlete(
  db: Connection,
  search: string
): Promise<AthleteI[]> {
  console.log("searchAthlete: ", search);
  const ar = await db.getRepository(Athlete);
  const athletes = await ar
    .createQueryBuilder("athlete")
    .where("Athlete_firstName like :name", { name: "%" + search + "%" })
    .orWhere("Athlete_lastName like :name", { name: "%" + search + "%" })
    .orWhere("Athlete_licenseNumber like :code", { code: "%" + search + "%" })
    .orWhere("Athlete_idNumber like :code", { code: "%" + search + "%" })
    .getMany();
  return athletes;
}
