export interface AthleteI {
  id: number;
  firstName: string;
  lastName: string;
  fatherName: string;
  motherName: string;
  telephone: string;
  address: string;
  email: string;
  town: string;
  postalCode: string;
  birthDate: Date;
  amka: string;
  idNumber: string;
  association: string;
  registrationDate: Date;
  licenseNumber: string;
  issueDate: Date;
  expirationDate: Date;
  issuingAuthority: string;
}

export interface InputAthleteI {
  firstName: string;
  lastName: string;
  fatherName: string;
  motherName: string;
  telephone: string;
  address: string;
  email: string;
  town: string;
  postalCode: string;
  birthDate: Date;
  amka: string;
  idNumber: string;
  association: string;
  registrationDate: Date;
  licenseNumber: string;
  issueDate: Date;
  expirationDate: Date;
  issuingAuthority: string;
}

export const AthleteTranslations = {
  id: 'ID',
  firstName: 'Όνομα',
  lastName: 'Επίθετο',
  fatherName: 'Όνομα Πατρός',
  motherName: 'Όνομα Μητρός',
  telephone: 'Τηλέφωνο',
  address: 'Διεύθυνση',
  email: 'Email',
  town: 'Πόλη',
  postalCode: 'ΤΚ',
  birthDate: 'Ημερομηνία Γέννησης',
  amka: 'ΑΜΚΑ',
  idNumber: 'Αριθμός ταυτότητας',
  association: 'Σωματείο',
  registrationDate: 'Ημερομηνία Εγγραφής',
  licenseNumber: 'Αριθμός Μητρώου',
  issueDate: 'Ημερομηνία έκδοσης',
  expirationDate: 'Ημερομηνία Λήξης',
  issuingAuthority: 'Εκδουσα Αρχή ',
};

export enum AthleteKeys {
  ID = 'id',
  FirstName = 'firstName',
  LastName = 'lastName',
  FatherName = 'fatherName',
  MotherName = 'motherName',
  Telephone = 'telephone',
  Address = 'address',
  Email = 'email',
  Town = 'town',
  PostalCode = 'postalCode',
  BirthDate = 'birthDate',
  Amka = 'amka',
  IdNumber = 'idNumber',
  Association = 'association',
  RegistrationDate = 'registrationDate',
  LicenseNumber = 'licenseNumber',
  IssueDate = 'issueDate',
  ExpirationDate = 'expirationDate',
  IssuingAuthority = 'issuingAuthority',
}
