import { _validateAmmoInput } from "./ammo.model";
import { InputAmmoI } from "./ammo.interface";

describe("validate ammo input", function() {
  it("validate letgth", function() {
    const ammo: InputAmmoI = {
      athleteID: 0,
      dateEntityID: 0,
      location: "",
      caliber: "",
      quantity: 0
    };
    let result = _validateAmmoInput(ammo);
    expect(result.length).toBe(4);
  });
});
