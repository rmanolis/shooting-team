import { InputAmmoI, AmmoI } from "./ammo.interface";
import {
  ErrorFieldI,
  ErrorI,
  PaginationI,
  ListPaginatedI
} from "./common.interface";
import { ErrValueIsEmpty, ErrValueIsZero, ErrFields } from "./common.errors";
import { Connection, getConnection } from "typeorm";
import { Ammo } from "../entities/ammo.entity";
import { getTotalNumberPages } from "./common.model";

export function _validateAmmoInput(input: InputAmmoI): ErrorFieldI[] {
  let errorFields: ErrorFieldI[] = [];

  for (let key in input) {
    if (typeof input[key] === "string") {
      if (input[key].length === 0) {
        errorFields.push({
          field: key,
          message: ErrValueIsEmpty.message
        });
      }
    }

    if (typeof input[key] === "number") {
      if (input[key] === 0) {
        errorFields.push({
          field: key,
          message: ErrValueIsZero.message
        });
      }
    }
  }
  if (errorFields.length > 0) {
    return errorFields;
  }

  return errorFields;
}

export async function addAmmo(
  db: Connection,
  input: InputAmmoI
): Promise<ErrorI | AmmoI> {
  const errs = _validateAmmoInput(input);
  if (errs.length > 0) {
    return {
      fields: errs,
      message: ErrFields.message
    };
  }

  const ar = await db.getRepository(Ammo);
  const am = new Ammo();
  am.athleteID = input.athleteID;
  am.caliber = input.caliber;
  am.createdDate = new Date();
  am.location = input.location;
  am.quantity = input.quantity;
  am.dateEntityID = input.dateEntityID;

  try {
    return await ar.save(am);
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function editAmmo(
  db: Connection,
  id: number,
  input: InputAmmoI
): Promise<ErrorI | AmmoI> {
  const errs = _validateAmmoInput(input);
  if (errs.length > 0) {
    return {
      fields: errs,
      message: ErrFields.message
    };
  }
  try {
    const amr = await db.getRepository(Ammo);
    const am = await amr.findOne({ id });
    if (am === undefined) {
      return {
        message: "Η καταχώρηση φυσιγγίων δεν υπάρχει με το ID " + id,
        fields: []
      };
    }
    const newAm = { ...am, ...input };
    return await amr.save(newAm);
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function deleteAmmo(
  db: Connection,
  id: number
): Promise<ErrorI | number> {
  try {
    const ar = await db.getRepository(Ammo);
    const am = await ar.findOne({ id });
    if (am === undefined) {
      return {
        message: "Η καταχώρηση φυσιγγίων δεν υπάρχει με το ID " + id,
        fields: []
      };
    }
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Ammo)
      .where("id = :id", { id })
      .execute();
    return id;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function getAmmo(
  db: Connection,
  id: number
): Promise<ErrorI | AmmoI> {
  try {
    const amr = await db.getRepository(Ammo);
    const am = await amr.findOne({ id });
    if (am === undefined) {
      return {
        message: "Η καταχώρηση φυσιγγίων δεν υπάρχει με το ID " + id,
        fields: []
      };
    }
    return am;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function getAmmosPaginated(
  db: Connection,
  pag: PaginationI
): Promise<ListPaginatedI<AmmoI>> {
  const ar = await db.getRepository(Ammo);
  const totalPageNumber = await getTotalNumberPages(db, Ammo, pag);
  const ams = await ar.find({
    skip: pag.page * pag.size,
    take: pag.size,
    order: {
      id: "DESC"
    }
  });
  const res: ListPaginatedI<AmmoI> = {
    page: pag.page,
    size: pag.size,
    totalPages: totalPageNumber,
    data: ams
  };
  return res;
}

export async function getAmmosPaginatedByDateID(
  db: Connection,
  dateEntityID: number,
  pag: PaginationI
): Promise<ListPaginatedI<AmmoI>> {
  const ar = await db.getRepository(Ammo);
  const totalPageNumber = await getTotalNumberPages(db, Ammo, pag);
  const ams = await ar.find({
    skip: pag.page * pag.size,
    take: pag.size,
    where: {
      dateEntityID
    },
    order: {
      id: "DESC"
    }
  });
  const res: ListPaginatedI<AmmoI> = {
    page: pag.page,
    size: pag.size,
    totalPages: totalPageNumber,
    data: ams
  };
  return res;
}
