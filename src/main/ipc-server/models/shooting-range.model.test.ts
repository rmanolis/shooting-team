import { _validateShootingRangeInput } from "./shooting-range.model";
import { InputShootingRangeI } from "./shooting-range.interface";

describe("validate shooting range input", function() {
  it("validate letgth", function() {
    const sr: InputShootingRangeI = {
      athleteID: 0,
      dateEntityID: 0,
      gunNumber: "",
      issuingAuthority: "",
      caliber: "",
      quantity: 0,
      warehouseQuantity: 0,
      isContest: false,
      isTraining: true
    };
    let result = _validateShootingRangeInput(sr);
    expect(result.length).toBe(5);
  });
});
