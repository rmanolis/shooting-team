import { ErrorFieldI, PaginationI, ListPaginatedI } from "./common.interface";
import { ErrValueIsEmpty, ErrValueIsZero } from "./common.errors";
import {
  InputShootingRangeI,
  ShootingRangeI
} from "./shooting-range.interface";
import { Connection, getConnection } from "typeorm";
import { ErrorI } from "./common.interface";
import { ErrFields } from "./common.errors";
import { ShootingRange } from "../entities/shooting-range.entity";
import { getTotalNumberPages } from "./common.model";

export function _validateShootingRangeInput(
  input: InputShootingRangeI
): ErrorFieldI[] {
  let errorFields: ErrorFieldI[] = [];

  for (let key in input) {
    if (typeof input[key] === "string") {
      if (input[key].length === 0) {
        errorFields.push({
          field: key,
          message: ErrValueIsEmpty.message
        });
      }
    }

    if (typeof input[key] === "number") {
      if (input[key] === 0) {
        errorFields.push({
          field: key,
          message: ErrValueIsZero.message
        });
      }
    }
  }
  if (errorFields.length > 0) {
    return errorFields;
  }

  return errorFields;
}

export async function addShootingRange(
  db: Connection,
  input: InputShootingRangeI
): Promise<ErrorI | ShootingRangeI> {
  const errs = _validateShootingRangeInput(input);
  if (errs.length > 0) {
    return {
      fields: errs,
      message: ErrFields.message
    };
  }

  const ssr = await db.getRepository(ShootingRange);
  const sr = new ShootingRange();
  sr.athleteID = input.athleteID;
  sr.dateEntityID = input.dateEntityID;
  sr.gunNumber = input.gunNumber;
  sr.caliber = input.caliber;
  sr.createdDate = new Date();
  sr.isContest = input.isContest;
  sr.isTraining = input.isTraining;
  sr.issuingAuthority = input.issuingAuthority;
  sr.quantity = input.quantity;
  sr.warehouseQuantity = input.warehouseQuantity;

  try {
    const newSr = await ssr.save(sr);
    return newSr;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function editShootingRange(
  db: Connection,
  id: number,
  input: InputShootingRangeI
): Promise<ErrorI | ShootingRangeI> {
  const errs = _validateShootingRangeInput(input);
  if (errs.length > 0) {
    return {
      fields: errs,
      message: ErrFields.message
    };
  }
  try {
    const ssr = await db.getRepository(ShootingRange);
    const shr = await ssr.findOne({ id });
    if (shr === undefined) {
      return {
        message: "Το σκοπευτήριο δεν υπάρχει με το ID " + id,
        fields: []
      };
    }
    const newShr = { ...shr, ...input };
    return await ssr.save(newShr);
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function deleteShootingRange(
  db: Connection,
  id: number
): Promise<ErrorI | number> {
  try {
    const ssr = await db.getRepository(ShootingRange);
    const ath = await ssr.findOne({ id });
    if (ath === undefined) {
      return {
        message: "Ο σκοπευτήριο δεν υπάρχει με το ID " + id,
        fields: []
      };
    }
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(ShootingRange)
      .where("id = :id", { id })
      .execute();
    return id;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function getShootingRange(
  db: Connection,
  id: number
): Promise<ErrorI | ShootingRangeI> {
  try {
    const ssr = await db.getRepository(ShootingRange);
    const shr = await ssr.findOne({ id });
    if (shr === undefined) {
      return {
        message: "Ο σκοπευτήριο δεν υπάρχει με το ID " + id,
        fields: []
      };
    }
    return shr;
  } catch (e) {
    const err = <Error>e;
    return { message: err.message, fields: [] };
  }
}

export async function getShootingRangePaginated(
  db: Connection,
  pag: PaginationI
): Promise<ListPaginatedI<ShootingRangeI>> {
  const sr = await db.getRepository(ShootingRange);
  const totalPageNumber = await getTotalNumberPages(db, ShootingRange, pag);
  const srs = await sr.find({
    skip: pag.page * pag.size,
    take: pag.size,
    order: {
      id: "DESC"
    }
  });
  const res: ListPaginatedI<ShootingRangeI> = {
    page: pag.page,
    size: pag.size,
    totalPages: totalPageNumber,
    data: srs
  };
  return res;
}

export async function getShootingRangePaginatedByDateID(
  db: Connection,
  dateEntityID: number,
  pag: PaginationI
): Promise<ListPaginatedI<ShootingRangeI>> {
  const ar = await db.getRepository(ShootingRange);
  const totalPageNumber = await getTotalNumberPages(db, ShootingRange, pag);
  const aths = await ar.find({
    skip: pag.page * pag.size,
    take: pag.size,
    where: {
      dateEntityID
    },
    order: {
      id: "DESC"
    }
  });
  const res: ListPaginatedI<ShootingRangeI> = {
    page: pag.page,
    size: pag.size,
    totalPages: totalPageNumber,
    data: aths
  };
  return res;
}
