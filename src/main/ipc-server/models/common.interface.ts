export interface ErrorI {
  message: string;
  fields: ErrorFieldI[];
}

export interface ErrorFieldI {
  field: string;
  message: string;
}

export interface PaginationI {
  page: number;
  size: number;
}

export interface ListPaginatedI<T> {
  page: number;
  totalPages: number;
  size: number;
  data: Array<T>;
}

export function isErrorI(v: any): boolean {
  return v.fields !== undefined && v.message !== undefined;
}
