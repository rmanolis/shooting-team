import { Connection } from 'typeorm';
import { PaginationI } from './common.interface';

export async function getTotalNumberPages(
  db: Connection,
  table: Function,
  pag: PaginationI,
): Promise<number> {
  const mr = await db.getRepository(table);
  const totalNumber = await mr.count();
  return Math.ceil(totalNumber / pag.size);
}
