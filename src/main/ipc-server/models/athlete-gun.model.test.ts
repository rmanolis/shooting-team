import { _validateAthleteGunInput } from './athlete-gun.model';
import { InputAthleteGunI } from './athlete-gun.interface';

describe('validate athlete gun input', function() {
  it('validate letgth', function() {
    const athg: InputAthleteGunI = {
      athleteID: 0,
      armsNumber: '',
      caliber: '',
    };
    let result = _validateAthleteGunInput(athg);
    expect(result.length).toBe(3);
  });
});
