export interface ShootingRangeI {
  id: number;
  athleteID: number;
  createdDate: Date;
  dateEntityID: number;
  gunNumber: string;
  issuingAuthority: string;
  caliber: string;
  quantity: number;
  warehouseQuantity: number;
  isContest: boolean;
  isTraining: boolean;
}

export interface InputShootingRangeI {
  athleteID: number;
  dateEntityID: number;
  gunNumber: string;
  issuingAuthority: string;
  caliber: string;
  quantity: number;
  warehouseQuantity: number;
  isContest: boolean;
  isTraining: boolean;
}

export const ShootingRangeTranslations = {
  id: "ID",
  athleteID: "Αθλητής",
  dateEntityID: "Ημέρα",
  gunNumber: "Αριθμός Όπλου",
  issuingAuthority: "Εκδουσα Αρχή ",
  quantity: "Ποσότητα",
  caliber: "Διαμέτρημα",
  warehouseQuantity: "Ποσότητα αποθήκης",
  isContest: "Είναι διαγωνισμός",
  isTraining: "Είναι εκπαίδευσης",
  createdDate: "Ημερομηνια δημιουργίας"
};

export enum ShootingRangeKeys {
  ID = "id",
  IssuingAuthority = "issuingAuthority",
  GunNumber = "gunNumber",
  Quantity = "quantity",
  Caliber = "caliber",
  WarehouseQuantity = "warehouseQuantity",
  IsContest = "isContest",
  IsTraining = "isTraining",
  CreatedDate = "createdDate",
  DateEntityID = "dateEntityID",
  AthleteID = "athleteID"
}
