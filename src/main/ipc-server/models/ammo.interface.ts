export interface AmmoI {
  id: number;
  athleteID: number;
  createdDate: Date;
  dateEntityID: number;
  location: string;
  caliber: string;
  quantity: number;
}

export interface InputAmmoI {
  athleteID: number;
  dateEntityID: number;
  location: string;
  caliber: string;
  quantity: number;
}

export const AmmoTranslations = {
  id: "ID",
  athleteID: "Αθλητής",
  dateEntityID: "Ημέρα",
  location: "Περιοχή",
  quantity: "Ποσότητα",
  caliber: "Διαμέτρημα"
};

export enum AmmoKeys {
  ID = "id",
  Quantity = "quantity",
  Caliber = "caliber",
  Location = "location",
  DateEntityID = "dateEntityID",
  AthleteID = "athleteID"
}
