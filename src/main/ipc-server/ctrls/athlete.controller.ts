import { Connection } from 'typeorm';
import { Event } from 'electron';
import * as am from '../models/athlete.model';
import { InputAthleteI } from '../models/athlete.interface';
import {
  ADD_ATHLETE_RESP,
  GET_ATHLETES_PAGINATED_RESP,
  EDIT_ATHLETE_RESP,
  DELETE_ATHLETE_RESP,
  GET_ATHLETE_RESP,
  SEARCH_ATHLETE_RESP,
} from '../const-routes';
import { PaginationI } from '../models/common.interface';

export class AthleteController {
  constructor(private db: Connection) {}

  // add-athlete
  // add-athlete-resp
  public add() {
    return async (event: Event, input: InputAthleteI) => {
      const athl = await am.addAthlete(this.db, input);
      await event.sender.send(ADD_ATHLETE_RESP, athl);
    };
  }

  // get-athletes-paginated
  // get-athletes-paginated-resp
  public getPaginated() {
    return async (event: Event, pg: PaginationI) => {
      const lmp = await am.getAthletesPaginated(this.db, pg);
      await event.sender.send(GET_ATHLETES_PAGINATED_RESP, lmp);
    };
  }

  // edit-athlete
  // edit-athlete-resp
  public editAthlete() {
    return async (event: Event, id: number, input: InputAthleteI) => {
      const ath = await am.editAthlete(this.db, id, input);
      await event.sender.send(EDIT_ATHLETE_RESP, ath);
    };
  }

  // delete-athlete
  // delete-athlete-resp
  public deleteAthlete() {
    return async (event: Event, id: number) => {
      const ath = await am.deleteAthlete(this.db, id);
      await event.sender.send(DELETE_ATHLETE_RESP, ath);
    };
  }

  // get-athlete
  // get-athlete-resp
  public getAthlete() {
    return async (event: Event, id: number) => {
      const ath = await am.getAthlete(this.db, id);
      await event.sender.send(GET_ATHLETE_RESP, ath);
    };
  }

  // search-athlete
  // search-athlete-resp
  public searchAthlete() {
    return async (event: Event, search: string) => {
      const ath = await am.searchAthlete(this.db, search);
      await event.sender.send(SEARCH_ATHLETE_RESP, ath);
    };
  }
}
