import { Connection } from 'typeorm';
import { Event } from 'electron';
import * as agm from '../models/athlete-gun.model';
import { InputAthleteGunI } from '../models/athlete-gun.interface';
import {
  ADD_ATHLETE_GUN_RESP,
  DELETE_ATHLETE_GUN_RESP,
  GET_TOTAL_ATHLETE_GUNS_RESP,
} from '../const-routes';

export class AthleteGunController {
  constructor(private db: Connection) {}
  // add-athlete-gun
  // add-athlete-gun-resp
  public addAthleteGun() {
    return async (event: Event, input: InputAthleteGunI) => {
      const athl = await agm.addAthleteGun(this.db, input);
      await event.sender.send(ADD_ATHLETE_GUN_RESP, athl);
    };
  }

  // delete-athlete-gun
  // delete-athlete-gun-resp
  public deleteAthleteGun() {
    return async (event: Event, id: number) => {
      const ath = await agm.deleteAthleteGun(this.db, id);
      await event.sender.send(DELETE_ATHLETE_GUN_RESP, ath);
    };
  }

  // get-total-athlete-guns
  // get-total-athlete-guns-resp
  public getTotalAthleteGuns() {
    return async (event: Event, id: number) => {
      const ath = await agm.getTotalAthleteGuns(this.db, id);
      await event.sender.send(GET_TOTAL_ATHLETE_GUNS_RESP, ath);
    };
  }
}
