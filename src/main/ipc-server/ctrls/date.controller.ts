import { Connection } from "typeorm";
import { Event } from "electron";
import * as dm from "../models/date.model";
import {
  GET_DATE_ENTITIES_PAGINATED_RESP,
  GET_DATE_ENTITY_FOR_TODAY_RESP
} from "../const-routes";

export class DateEntityController {
  constructor(private db: Connection) {}

  public getPaginated() {
    return async (event: Event, limit: number, lastId: number | null) => {
      const lmp = await dm.getDateEntitiesLimited(this.db, limit, lastId);
      await event.sender.send(GET_DATE_ENTITIES_PAGINATED_RESP, lmp);
    };
  }

  public getToday() {
    return async (event: Event) => {
      const out = await dm.getDateEntityForToday(this.db);
      await event.sender.send(GET_DATE_ENTITY_FOR_TODAY_RESP, out);
    };
  }
}
