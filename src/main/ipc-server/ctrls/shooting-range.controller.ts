import { Connection } from 'typeorm';
import { Event } from 'electron';
import { InputShootingRangeI } from '../models/shooting-range.interface';
import * as srm from '../models/shooting-range.model';
import {
  ADD_SHOOTING_RANGE_RESP,
  GET_SHOOTING_RANGES_PAGINATED_RESP,
  EDIT_SHOOTING_RANGE_RESP,
  DELETE_SHOOTING_RANGE_RESP,
  GET_SHOOTING_RANGE_RESP,
  GET_SHOOTING_RANGES_BY_DATE_PAGINATED_RESP,
} from '../const-routes';
import { PaginationI } from '../models/common.interface';

export class ShootingRangeController {
  constructor(private db: Connection) {}

  public add() {
    return async (event: Event, input: InputShootingRangeI) => {
      const sr = await srm.addShootingRange(this.db, input);
      await event.sender.send(ADD_SHOOTING_RANGE_RESP, sr);
    };
  }

  public getPaginated() {
    return async (event: Event, pg: PaginationI) => {
      const lmp = await srm.getShootingRangePaginated(this.db, pg);
      await event.sender.send(GET_SHOOTING_RANGES_PAGINATED_RESP, lmp);
    };
  }

  public getPaginatedByDateId() {
    return async (event: Event, forDateId: number, pg: PaginationI) => {
      const lmp = await srm.getShootingRangePaginatedByDateID(
        this.db,
        forDateId,
        pg,
      );
      await event.sender.send(GET_SHOOTING_RANGES_BY_DATE_PAGINATED_RESP, lmp);
    };
  }

  public edit() {
    return async (event: Event, id: number, input: InputShootingRangeI) => {
      const sr = await srm.editShootingRange(this.db, id, input);
      await event.sender.send(EDIT_SHOOTING_RANGE_RESP, sr);
    };
  }

  public delete() {
    return async (event: Event, id: number) => {
      const sr = await srm.deleteShootingRange(this.db, id);
      await event.sender.send(DELETE_SHOOTING_RANGE_RESP, sr);
    };
  }

  public get() {
    return async (event: Event, id: number) => {
      const sr = await srm.getShootingRange(this.db, id);
      await event.sender.send(GET_SHOOTING_RANGE_RESP, sr);
    };
  }
}
