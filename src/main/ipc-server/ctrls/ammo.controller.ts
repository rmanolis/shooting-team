import { Connection } from "typeorm";
import { Event } from "electron";
import { InputAmmoI } from "../models/ammo.interface";
import * as am from "../models/ammo.model";
import { PaginationI } from "../models/common.interface";
import {
  ADD_AMMO_RESP,
  GET_AMMO_PAGINATED_RESP,
  GET_AMMOS_BY_DATE_PAGINATED_RESP,
  EDIT_AMMO_RESP,
  DELETE_AMMO_RESP,
  GET_AMMO_RESP
} from "../const-routes";

export class AmmoController {
  constructor(private db: Connection) {}

  public add() {
    return async (event: Event, input: InputAmmoI) => {
      const a = await am.addAmmo(this.db, input);
      await event.sender.send(ADD_AMMO_RESP, a);
    };
  }

  public getPaginated() {
    return async (event: Event, pg: PaginationI) => {
      const lmp = await am.getAmmosPaginated(this.db, pg);
      await event.sender.send(GET_AMMO_PAGINATED_RESP, lmp);
    };
  }

  public getPaginatedByDateId() {
    return async (event: Event, forDateId: number, pg: PaginationI) => {
      const lmp = await am.getAmmosPaginatedByDateID(this.db, forDateId, pg);
      await event.sender.send(GET_AMMOS_BY_DATE_PAGINATED_RESP, lmp);
    };
  }

  public edit() {
    return async (event: Event, id: number, input: InputAmmoI) => {
      const sr = await am.editAmmo(this.db, id, input);
      await event.sender.send(EDIT_AMMO_RESP, sr);
    };
  }

  public delete() {
    return async (event: Event, id: number) => {
      const sr = await am.deleteAmmo(this.db, id);
      await event.sender.send(DELETE_AMMO_RESP, sr);
    };
  }

  public get() {
    return async (event: Event, id: number) => {
      const sr = await am.getAmmo(this.db, id);
      await event.sender.send(GET_AMMO_RESP, sr);
    };
  }
}
